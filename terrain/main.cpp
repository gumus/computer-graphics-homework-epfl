/** ICG 2015 - Group 10
 * Orçun Gumus
 * Kivanc Kamay
 * Simon Rodriguez
 */

#include "icg_common.h"

#include "trackball.h"
#include "_grid/grid.h"
#include "FrameBuffer.h"
#include "_noisequad/NoiseQuad.h"
#include "_light/light.h"
#include "_water/water.h"
#include "_screenquad/ScreenQuad.h"
#include "_skybox/skybox.h"
#include "_skydome/skydome.h"
#include "_clouds/Clouds.h"
#include "_particles/particles.h"
#include "_bezier/bezier.h"
#include "_point/point.h"
#include "_balloon/balloon.h"

#ifdef WITH_ANTTWEAKBAR
#include <AntTweakBar.h>
TwBar *bar;
#endif

using namespace std;
typedef Eigen::Transform<float,3,Eigen::Affine> Transform;

//Headers
vec2 transform_screen_coords(int x, int y);
void mouse_pos(int x, int y);
void mouse_button(int button, int action);
float mix(float from, float to, float alpha);
int signum(float value);
void initBezier();
void selection_button(int button, int action);
#ifdef WITH_ANTTWEAKBAR
void TW_CALL regen_noise(void *clientData);
void TW_CALL set_rain(void *clientData);
#endif


//Sizes
int WIDTH = 800;
int HEIGHT = 600;
#define FB_SIZE 512
int GRID_SIZE = FB_SIZE/4;

//Matrices
mat4 projection_matrix;
mat4 view_matrix;
mat4 final_view_matrix;
mat4 reverse_view_matrix;
mat4 trackball_matrix;
mat4 old_trackball_matrix;
mat4 shadow_projection_matrix;
mat4 shadow_view_matrix;
mat4 offset_matrix;

//Buffers
FrameBuffer fb(FB_SIZE,FB_SIZE);
NoiseQuad noise_squad(FB_SIZE,FB_SIZE);
FrameBuffer fb_wo_water(WIDTH,HEIGHT);
FrameBuffer fb_under_water(WIDTH,HEIGHT);
FrameBuffer fb_shadow_map(FB_SIZE,FB_SIZE);
GLuint noise_tex;
GLfloat height_map[FB_SIZE * FB_SIZE];

//Objects
Cloud clouds;
Grid grid;
Grid grid_normal;
Light sun;
Water water;
Skybox skybox;
Skydome skydome;
Particles particles;
Balloon balloon;
bool show_ballon = true;
vec3 balloon_pos;

//Sky settings
int sky = 2;
bool cloud = false;
bool rainy = false;
float weather = 1.0;
float previousTime = 0.0;

bool shadow_map_enabled = true;

//Camera settings
enum CameraMode {TRACKBALL_MODE, FLIGHT_MODE, FPS_MODE, BEZIER_MODE, BALLOON_MODE};
Trackball trackball;
CameraMode camera_mode = TRACKBALL_MODE;
vec3 camera_pos;
vec3 look_at;
vec3 up;
vec2 start_pos;
float inertia = 0.075;
float velocity = 0.0f;
float lateral_velocity = 0.0;//<angle
float vertical_velocity = 0.0;//<angle
float velocity_increment = 0.1;
float angular_increment = 0.3;
float start_time = 0.0;
bool cycle = false;
float previous_y;
float fov = 45.0;

//Bezier paths
GLuint _pid_bezier;
GLuint _pid_point;
GLuint _pid_point_selection;
BezierCurve cam_pos_curve;
BezierCurve cam_look_curve;
std::vector<ControlPoint> cam_pos_points;
std::vector<ControlPoint> cam_look_points;
int selected_point = -1;
vec3 start_cam_pos;
vec3 start_cam_look;
float previous_t = 0.0;
bool constant_speed = true;
bool show_bezier = false;
float fixed_velocity = 0.01;

///---------INIT------------
//Called once at launch
void init(){

    // Enable depth test.
    glEnable(GL_DEPTH_TEST);

    //Init bezier curves
    initBezier();

    //Framebuffer init
    noise_tex = fb.init(true,true);
    noise_squad.init();

    GLuint scene_wo_water = fb_wo_water.init();
    GLuint scene_under_water = fb_under_water.init();
    fb_shadow_map.depth_only = true;
    GLuint shadow_map = fb_shadow_map.init();
    check_error_gl();

    //Geometry init

    grid.init(noise_tex,GRID_SIZE,shadow_map);
    skybox.init();
    skydome.init();
    clouds.init();
    particles.init();
    balloon.init();
    balloon_pos = vec3(0,0,0);
    water.init(scene_wo_water,scene_under_water,noise_tex,shadow_map);
    check_error_gl();

    //Camera init
    camera_pos = vec3(0.0f, 0.0f, -1.0f);
    up = vec3(0.0,1.0,0.0);
    look_at = vec3(0.0,0.0,0.0);
    view_matrix = Eigen::Affine3f(Eigen::Translation3f(camera_pos)).matrix();
    reverse_view_matrix = Eigen::lookAt(vec3(camera_pos[0],-camera_pos[1],camera_pos[2]),look_at,up);
    projection_matrix = Eigen::perspective(45.0f, WIDTH / float(HEIGHT), 0.005f,50.0f).matrix();
    shadow_projection_matrix = Eigen::ortho(-1.5f, 1.5f, -1.5f, 1.5f, 0.1f, 16.0f);
    trackball_matrix = mat4::Identity();

    //Antweakbar init
    #ifdef WITH_ANTTWEAKBAR
    TwInit(TW_OPENGL_CORE, NULL);
    TwWindowSize(WIDTH, HEIGHT);
    bar = TwNewBar("Settings");


    //TwAddVarRW(bar, "Balloon", TW_TYPE_BOOL8, &show_ballon,"group='Misc.'");
    ///-----Camera
     TwAddVarRW(bar, "FOV", TW_TYPE_FLOAT, &fov, "min=10.0 max=130.0 step=5.0 group='Camera'");
    TwAddVarRW(bar, "Fixed speed", TW_TYPE_FLOAT, &fixed_velocity, "min=0.0 max=1.0 step=0.01 group='Camera'");
    TwAddVarRW(bar, "Inertia", TW_TYPE_FLOAT, &inertia, "min=0.0 max=1.0 step=0.01 group='Camera'");
    TwAddVarRW(bar, "Velocity incr.", TW_TYPE_FLOAT, &velocity_increment, "min=0.0 max=5.0 step=0.1 group='Camera'");
    TwAddVarRW(bar, "Angular vel. incr.", TW_TYPE_FLOAT, &angular_increment, "min=0.0 max=5.0 step=0.1 group='Camera'");
    TwAddVarRW(bar, "Show bezier", TW_TYPE_BOOL8, &show_bezier,"group='Camera'");
    TwAddVarRW(bar, "Constant bez. speed", TW_TYPE_BOOL8, &constant_speed,"group='Camera'");
    TwAddSeparator(bar, NULL, "group='Camera'");
    ///-----Light
    TwAddVarRW(bar, "Shadow map", TW_TYPE_BOOL8, &shadow_map_enabled,"group='Light'");
    TwAddVarRW(bar, "Bias", TW_TYPE_FLOAT, &grid.bias, "min=0.0 max=5.0 step=0.001 group='Light'");
    TwAddVarRW(bar, "LightDir", TW_TYPE_DIR3F, &sun.light_pos, "group='Light'");

    ///-----Sky
    TwAddVarRW(bar, "Type", TW_TYPE_INT16, &sky, "min=1 max=2 step=1 group='Sky'");
    TwAddVarRW(bar, "Time", TW_TYPE_BOOL8, &cycle,"group='Sky'");
    TwAddButton(bar, "Rainy", set_rain, NULL, "group='Sky'");
    TwAddVarRW(bar, "Cloud noise", TW_TYPE_BOOL8, &cloud,"group='Sky'");
    ///-----Terrain
    TwAddButton(bar, "Regenerate", regen_noise, NULL, "group='Terrain'");
    TwAddVarRW(bar, "Mode", TW_TYPE_INT16, &noise_squad.mode, "min=0 max=8 step=1 group='Terrain'");
    TwAddVarRW(bar, "Number of cells", TW_TYPE_INT16, &noise_squad.number_cells, "min=1 max=20 step=1 group='Terrain'");
    TwAddVarRW(bar, "Octaves", TW_TYPE_INT16, &noise_squad.octaves, "min=1 max=30 step=1 group='Terrain'");
    TwAddVarRW(bar, "Lacunarity", TW_TYPE_FLOAT, &noise_squad.lacunarity, "min=0.0 max=10.0 step=0.1 group='Terrain'");
    TwAddVarRW(bar, "H", TW_TYPE_FLOAT, &noise_squad.h, "min=0.0 max=1.0 step=0.1 group='Terrain'");
    TwAddVarRW(bar, "Offset", TW_TYPE_FLOAT, &noise_squad.offset, "min=0.0 max=20.0 step=0.1 group='Terrain'");
    TwAddVarRW(bar, "Gain", TW_TYPE_FLOAT, &noise_squad.gain, "min=0.0 max=20.0 step=0.1 group='Terrain'");
    TwAddVarRW(bar, "Scale factor", TW_TYPE_FLOAT, &grid.scale_factor, "min=0.0 max=1.5 step=0.05 group='Terrain'");
    ///--- Callbacks
    glfwSetMouseButtonCallback(mouse_button);
    glfwSetMousePosCallback(mouse_pos);
    glfwSetMouseWheelCallback((GLFWmousewheelfun)TwEventMouseWheelGLFW);
    #endif

    // Matrix that can be used to move a point's components from [-1, 1] to [0, 1].
    offset_matrix << 0.5f, 0.0f, 0.0f, 0.5f,
                     0.0f, 0.5f, 0.0f, 0.5f,
                     0.0f, 0.0f, 0.5f, 0.5f,
                     0.0f, 0.0f, 0.0f, 1.0f;

    previousTime = glfwGetTime();
}

//Initializes the Bezier curves
void initBezier(){
    //Shaders for bezier
    _pid_bezier = opengp::load_shaders("_bezier/bezier_vshader.glsl", "_bezier/bezier_fshader.glsl");
   if(!_pid_bezier) exit(EXIT_FAILURE);
    _pid_point = opengp::load_shaders("_point/point_vshader.glsl", "_point/point_fshader.glsl");
       if(!_pid_point) exit(EXIT_FAILURE);
    _pid_point_selection = opengp::load_shaders("_point/point_selection_vshader.glsl", "_point/point_selection_fshader.glsl");
       if(!_pid_point_selection) exit(EXIT_FAILURE);

    // Sets background color.
    glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
    glClearColor(0.38,0.73,1.0,1.0);
    //Bezier paths and control points init
    ///--- init cam_pos_curve
        cam_pos_curve.init(_pid_bezier,0);

        cam_pos_points.push_back(ControlPoint(-0.98, 0.7,1.414, 0));
        cam_pos_points.push_back(ControlPoint(-1.386, 0.7, -1.498, 1));
        cam_pos_points.push_back(ControlPoint(2.03,  0.56, -1.414,2));
        cam_pos_points.push_back(ControlPoint(1.064,0.42, 0.938,  3));
        cam_pos_points.push_back(ControlPoint(0.406,  0.28,1.372, 4));
        cam_pos_points.push_back(ControlPoint(-1.008,  0.21,1.442, 5));
        cam_pos_points.push_back(ControlPoint(-0.616,  0.14, -3.156,6));

        for (unsigned int i = 0; i < cam_pos_points.size(); i++) {
            cam_pos_points[i].id() = i;
            cam_pos_points[i].init(_pid_point, _pid_point_selection);
        }
        cam_pos_curve.set_points(cam_pos_points);
        cam_look_curve.init(_pid_bezier,1);

           cam_look_points.push_back(ControlPoint(-0.19,  0.15,-0.02, 0));
           cam_look_points.push_back(ControlPoint(-0.18,  0.15,-0.32, 1));
           cam_look_points.push_back(ControlPoint(0.27, 0.17, -0.24, 2));
           cam_look_points.push_back(ControlPoint(-0.01,  0.3,0.22, 3));
           cam_look_points.push_back(ControlPoint(-0.252, 0.39,  0.507,4));
           cam_look_points.push_back(ControlPoint(-0.496,  0.47,-0.042, 5));
           cam_look_points.push_back(ControlPoint(-0.03,  0.48,-0.34, 6));

           for (unsigned int i = 0; i < cam_look_points.size(); i++) {
               //We shift indices by cam_pos_points.size(), to be sure no point on cam_look will have the same id as a point of cam_pos
               cam_look_points[i].id() = i + cam_pos_points.size();
               cam_look_points[i].init(_pid_point, _pid_point_selection);
           }
           cam_look_curve.set_points(cam_look_points);

}

///---------TERRAIN GENERATION------------
//Generate noise
void generate_noise(){
    fb.clear();
    fb.bind();
        glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
        noise_squad.draw();
        glBindTexture(GL_TEXTURE_2D, noise_tex);

        glGetTexImage(GL_TEXTURE_2D, 0,GL_RED,GL_FLOAT, height_map);

    fb.unbind();
    glViewport(0, 0, WIDTH, HEIGHT);
    glClearColor(0.38,0.73,1.0,1.0);
}

//Returns the height of the terrain at position x,y,
float heightAtPosition(float x, float z){
    int i0 = int(x);
    int i1 = i0+1;
    int j0 = int(z);
    int j1 = j0+1;
    float h01 = height_map[j0*FB_SIZE+i0]*grid.scale_factor;
    float h02 = height_map[j0*FB_SIZE+i1]*grid.scale_factor;
    float h03 = height_map[j1*FB_SIZE+i0]*grid.scale_factor;
    float h04 =  height_map[j1*FB_SIZE+i1]*grid.scale_factor;
    float h11 = mix(h01,h02,x-i0);
    float h12 = mix(h03,h04,x-i0);
    return mix(h11,h12,z-j0);
}

///---------MAIN LOOP

//Update camera position, look_at and up.
void updatePositions(float deltaTime){
    //std::cout << "Delta time: "<<deltaTime << ", velocity: " << velocity << ", lat. velocity: "<<lateral_velocity<<", vert. velocity: "<<vertical_velocity<<"."<<std::endl;
    switch (camera_mode) {
    case TRACKBALL_MODE:{
        vec4 _cam_pos_1 = (view_matrix* trackball_matrix).inverse() * vec4(0,0,0,1);
        camera_pos = vec3(_cam_pos_1[0],_cam_pos_1[1],_cam_pos_1[2]);
        //In trackball mode, look_at = vec3(0,0,0)
        break;
    }
    case BEZIER_MODE:{
                vec3 cam_pos = start_cam_pos;
                vec3 cam_look = start_cam_look;
                up = vec3(0.0f, 1.0f, 0.0f);

                //We compute t according to the current time and the desired duration
                float velocity_1 = velocity*0.5;
                if(constant_speed){
                    velocity_1 = fixed_velocity;
                }
                double t = previous_t + deltaTime*velocity_1;

                //We need to loop the animation correctly when t reach 1
                if (t>1.0f){
                    t=0.0f;
                }
                previous_t = t;
                //Sampling
                cam_pos_curve.sample_point(t,cam_pos);
                cam_look_curve.sample_point(t,cam_look);
                camera_pos = cam_pos;
                look_at = cam_look;
        break;
    }
    default:{
        //First, move along direction
        vec3 direction =  (look_at - camera_pos).normalized();
        camera_pos = camera_pos + direction * deltaTime * velocity;

        if(camera_mode==FPS_MODE){
            float x = camera_pos[0];
            float z = camera_pos[2];
            //std::cout << "Cam_orig: x: "<<x <<", z: "<<z<<std::endl;
            if(x<1.0 && x>-1.0 && z < 1.0 && z>-1.0){
                x+=1.0;
                x*=FB_SIZE*0.5;
                z=-z;
                z+=1.0;
                z*=FB_SIZE*0.5;
               // std::cout << "Cam: x: "<<x <<", z: "<<z<<std::endl;
                camera_pos[1]=fmax(0,heightAtPosition(x,z));
            } else {
                camera_pos[1]=0.0;
            }
            camera_pos[1]=camera_pos[1]+0.02;

        } else if(camera_mode==BALLOON_MODE){
               camera_pos = balloon_pos+vec3(0.0,0.012,0.0);
        }

        look_at = camera_pos + direction;


        //The direction hasn't changed
        vec3 lateral_direction = direction.cross(up).normalized();

        //Vertical rotation
        vec3 vertical_direction = lateral_direction.cross(direction).normalized();
        look_at = camera_pos + direction * cos(vertical_velocity) * deltaTime + vertical_direction * sin(vertical_velocity) * deltaTime;

        //Recomputing up (lateral_direction not modified)
        direction =  (look_at - camera_pos).normalized();
        up = (lateral_direction.cross(direction)).normalized();

        //We do the lateral rotation in the world horizontal plane
        vec3 vertical = vec3(0,1,0);
        mat3 rotation = Eigen::Affine3f(Eigen::AngleAxisf(-deltaTime*lateral_velocity,vec3(0,1.0,0))).matrix().block(0,0,3,3);
        vec3 result = rotation * (look_at - camera_pos);
        look_at =camera_pos + result;

        //Updating lateral direction after horizontal rotation
        direction =  (look_at - camera_pos).normalized();

        //up is an approximation, the genral direction is enough. We avoid getting flipped.
        up = vec3(0,1,0);
        break;}
    }

    //We dont want to change the sign
    float magnitude = fmax(abs(velocity)-inertia,0);
    velocity=signum(velocity)*magnitude;
    float lateral_magnitude = fmax(abs(lateral_velocity)-inertia*2.0,0);
    lateral_velocity=signum(lateral_velocity)*lateral_magnitude;
    float vertical_magnitude = fmax(abs(vertical_velocity)-inertia,0);
    vertical_velocity=signum(vertical_velocity)*vertical_magnitude;


}

//Gets called for every frame.
void display(){

    //Reset
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
    water.scale_factor = grid.scale_factor;

     //We need a fixed time for some parts of the rendering
    float time = glfwGetTime();

    //Balloon
    //We need to compute balloon position before computing camera position in the case where we use the BALLOON camera mode
    mat4 balloon_model_matrix = (Transform::Identity()).matrix();
    if(show_ballon){
        Transform _M = Transform::Identity();
        float scale = 0.02;
        vec3 pos = vec3(0.4*cos(time*0.1),0.0,0.4*sin(time*0.1));
        float x = pos[0]+1.0;
        x*=FB_SIZE*0.5;
        float z = -pos[2];
        z+=1.0;
        z*=FB_SIZE*0.5;
        pos[1] = fmax(0.1,heightAtPosition(x,z))+0.1;
        balloon_pos = pos;
        _M *= Eigen::Translation3f(pos[0],pos[1],pos[2]);
        _M *= Eigen::AlignedScaling3f(scale, scale, scale);
        balloon_model_matrix = _M.matrix();

    }

    //CAMERA COMPUTATIONS
    updatePositions(time-previousTime);

    //Camera matrix
    vec3 vertical = vec3(0,1,0);
    vec3 reverse_camera_pos = camera_pos - 2.0* vertical.dot(camera_pos)*vertical;
    vec3 reverse_look_at = look_at - 2.0*vertical.dot(look_at)*vertical;
    reverse_view_matrix = Eigen::lookAt(reverse_camera_pos,reverse_look_at, up);
    final_view_matrix = Eigen::lookAt(camera_pos,look_at, up);
    projection_matrix = Eigen::perspective(fov, WIDTH / float(HEIGHT), 0.005f,50.0f).matrix();

    //MATRICES COMPUTATIONS
    //Terrain and water
    mat4 quad_model_matrix = Eigen::Affine3f(Eigen::Translation3f(vec3(0.0f, -0.0f, 0.0f))).matrix();
     mat4 water_model_matrix = (Transform::Identity()*Eigen::AlignedScaling3f(18.0,18.0,18.0)).matrix();
    Transform _sky = Transform::Identity();//Eigen::Affine3f(Eigen::Translation3f(vec3(0.0f, 0.0f, 4.0f)));
    _sky = _sky * Eigen::AlignedScaling3f(30.0,30.0,30.0) ;
    mat4 skybox_model_matrix = _sky.matrix();

    //Sun position
    if(cycle){
        mat3 rotation = (Eigen::AngleAxisf(0.55, vec3(1,0,0))*Transform::Identity()).matrix().block<3,3>(0,0);
        sun.light_pos = rotation*10*vec3(cos(time*0.3),0.0,sin(time*0.3));
    }

    //Weather
    //(0.5 = rainy, 1.0 = nice, smooth temporal transition between the two)
    if (rainy){
        weather-=0.01;
    } else {
        weather+=0.01;
    }
    weather = fmin(fmax(weather,0.5),1.0);

    //DRAWING SHADOW MAP
    mat4 shadow_matrix = (Transform::Identity()).matrix();
    if(shadow_map_enabled){
        shadow_matrix = offset_matrix*shadow_projection_matrix*shadow_view_matrix;
        fb_shadow_map.bind();
            glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
            vec3 pos = sun.light_pos.normalized();
            shadow_view_matrix = Eigen::lookAt(pos,vec3(0,0,0),vec3(0,1,0));
            grid.draw(quad_model_matrix,shadow_view_matrix,shadow_projection_matrix,sun,shadow_matrix,false,true);
            if(show_ballon){
                balloon.draw(balloon_model_matrix,shadow_view_matrix,shadow_projection_matrix,sun);
            }
            check_error_gl();
        fb_shadow_map.unbind();
        check_error_gl();
        water.bias = grid.bias;
    }

    //DRAWING FROM UNDERWATER
    fb_under_water.bind(WIDTH,HEIGHT);
        glClearColor(0.38,0.73,1.0,1.0);
        glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
        //Sky type
        if(sky==1){
            skybox.draw(skybox_model_matrix, reverse_view_matrix, projection_matrix,sun);
        } else {
            skydome.draw(skybox_model_matrix, reverse_view_matrix, projection_matrix,sun,weather,true);
        }
        //Clouds on a plane over the terrain
        if(cloud){
            glEnable( GL_BLEND );
            clouds.draw(quad_model_matrix,  reverse_view_matrix  , projection_matrix,time);
            glDisable( GL_BLEND );
        }
        check_error_gl();

        //Rendering anything else except water, and the terrain is clipped from below
        grid.draw(quad_model_matrix, reverse_view_matrix  , projection_matrix, sun,shadow_matrix,true);
        if(show_ballon){
            balloon.draw(balloon_model_matrix,reverse_view_matrix,projection_matrix,sun);
        }

    fb_under_water.unbind();
    check_error_gl();

    //DRAWING WITHOUT WATER
    vec3 deep_color = sun.getDeepColor();
    fb_wo_water.bind(WIDTH,HEIGHT);
        glClearColor(deep_color[0],deep_color[1],deep_color[2],1.0);
        glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
        //Rendering the grid on a dark water background
        grid.draw(quad_model_matrix, final_view_matrix  , projection_matrix, sun,shadow_matrix);
        check_error_gl();
    fb_wo_water.unbind();
    check_error_gl();

    //FINAL SCENE DRAWING
    glClearColor(0.38,0.73,1.0,1.0);
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
    //Sky type
    if(sky==1){
        skybox.draw(skybox_model_matrix, final_view_matrix, projection_matrix,sun);
    } else {
        skydome.draw(skybox_model_matrix, final_view_matrix, projection_matrix,sun,weather,true);
    }
    //Plane with clouds
    if(cloud){
        glEnable( GL_BLEND );
        clouds.draw(quad_model_matrix, final_view_matrix  , projection_matrix,time);
        glDisable( GL_BLEND );
    }
    grid.draw( quad_model_matrix, final_view_matrix  , projection_matrix, sun,shadow_matrix);
    if(show_ballon){
        balloon.draw(balloon_model_matrix,final_view_matrix,projection_matrix,sun);
    }
    //grid_normal.draw( quad_model_matrix, final_view_matrix  , projection_matrix, sun);///<disable to hide normal

    water.draw( water_model_matrix, final_view_matrix , projection_matrix, sun,sqrt(camera_pos[0]*camera_pos[0]+camera_pos[2]*camera_pos[2])/camera_pos.norm(),shadow_matrix);
    particles.draw(final_view_matrix,projection_matrix,camera_pos,time-previousTime);

    check_error_gl();
    //Antweakbar display
#ifdef WITH_ANTTWEAKBAR
    TwDraw();
#endif
    check_error_gl();

    //Drawing the bezier curves
    if (camera_mode == TRACKBALL_MODE && show_bezier) {
        mat4 points_model_matrix = (Transform::Identity()).matrix();
            for (unsigned int i = 0; i < cam_pos_points.size(); i++) {
                cam_pos_points[i].draw(points_model_matrix , final_view_matrix, projection_matrix);
            }
            for (unsigned int i = 0; i < cam_look_points.size(); i++) {
               cam_look_points[i].draw(points_model_matrix, final_view_matrix, projection_matrix);
            }
          cam_pos_curve.draw(points_model_matrix , final_view_matrix, projection_matrix);
          cam_look_curve.draw(points_model_matrix , final_view_matrix, projection_matrix);
    }
    previousTime = time;
}

///---------GENERAL INPUTS---------

//Called when key pressed
void keyboard(int key, int action){
    if(action == GLFW_RELEASE) return;
    glfwEnable(GLFW_KEY_REPEAT);

    switch(key){
    case '1'://Camera mode : trackball
        camera_mode = TRACKBALL_MODE;
        up = vec3(0.0,1.0,0.0);
        look_at = vec3(0.0,0.0,0.0);
        camera_pos = vec3(0.0f, 0.0f, -1.0f);
        break;
    case '2'://Camera mode : fly mode
        camera_mode = FLIGHT_MODE;
        break;
    case '3'://Camera mode : fps mode
        camera_mode = FPS_MODE;
        break;
    case '4'://Camera mode : Bezier paths
        camera_mode = BEZIER_MODE;
        start_time = glfwGetTime();
        previous_t = 0.0;
        start_cam_look = look_at;
        start_cam_pos = camera_pos;
        break;
    case '5'://Camera mode : in the balloon
        camera_mode = BALLOON_MODE;
        break;
    case 'W'://FORWARD
        velocity +=velocity_increment;
        break;
    case 'S'://BACKWARD
        velocity -=velocity_increment;
        break;
    case 'A'://ROTATE LEFT
        lateral_velocity -= angular_increment;
        break;
    case 'D'://ROTATE RIGHT
        lateral_velocity += angular_increment;
        break;
    case 'Q'://ROTATE UP
        vertical_velocity += velocity_increment;
        break;
    case 'E'://ROTATE DOWN
        vertical_velocity -= velocity_increment;
        break;
    case GLFW_KEY_F1:
        fb.display_color_attachment("Noise");
        break;
    case GLFW_KEY_F2:
        fb_wo_water.display_color_attachment("Transmission");
        break;
    case GLFW_KEY_F3:
        fb_under_water.display_color_attachment("Reflection");
        break;
    case GLFW_KEY_F4:
        fb_shadow_map.display_color_attachment("Shadow map");
        break;
    default:
        break;
    }
}

// Transforms glfw screen coordinates into normalized OpenGL coordinates.
vec2 transform_screen_coords(int x, int y) {
    //Added a min and a max to ensure (x,y) stay in [-1,1] even if the cursor slides out of the window during a drag.
    return vec2(max(min(1.0f,2.0f * (float)x / WIDTH - 1.0f),-1.0f),
                max(min(1.0f,1.0f - 2.0f * (float)y / HEIGHT),-1.0f));
}

void mouse_button(int button, int action) {
    if (!TwEventMouseButtonGLFW(button, action)){
       if (camera_mode == TRACKBALL_MODE){
            if (button == GLFW_MOUSE_BUTTON_LEFT && action == GLFW_PRESS && (glfwGetKey(GLFW_KEY_LALT) == 0)&& (glfwGetKey(GLFW_KEY_LSHIFT) == 0)) {
                int x_i, y_i;
                glfwGetMousePos(&x_i, &y_i);
                vec2 p = transform_screen_coords(x_i, y_i);
                trackball.begin_drag(p.x(), p.y());
                old_trackball_matrix = trackball_matrix;  // Store the current state of the model matrix.
            }else if ((button == GLFW_MOUSE_BUTTON_RIGHT && action == GLFW_PRESS) ||((button == GLFW_MOUSE_BUTTON_LEFT) && (action == GLFW_PRESS) && (glfwGetKey(GLFW_KEY_LALT) == 1))) {
                //When the right button is pressed, a zoom will begin :
                //  we need to register the y vposition of the cursor to calculate a delta in mouse_pos.
                int x_i, y_i;
                glfwGetMousePos(&x_i, &y_i);
                vec2 p = transform_screen_coords(x_i, y_i);
                previous_y = p[1];
            } else if (button == GLFW_MOUSE_BUTTON_LEFT && (glfwGetKey(GLFW_KEY_LSHIFT) == 1) && show_bezier){
                //We are trying to select a bezier control point by left clicking + left shift key
                selection_button(button, action);
            }
        }
    }
}

void mouse_pos(int x, int y) {
    if( !TwEventMousePosGLFW(x, y) )  // send event to AntTweakBar
        { // event has not been handled by AntTweakBar

        if (camera_mode == TRACKBALL_MODE){
            if (glfwGetMouseButton(GLFW_MOUSE_BUTTON_LEFT) == GLFW_PRESS && glfwGetKey(GLFW_KEY_LALT) == 0 && glfwGetKey(GLFW_KEY_LSHIFT) == 0) {
                vec2 p = transform_screen_coords(x, y);
                trackball_matrix = trackball.drag(p.x(),p.y()) * old_trackball_matrix;
            }
            // Zoom
            if (glfwGetMouseButton(GLFW_MOUSE_BUTTON_RIGHT) == GLFW_PRESS ||(glfwGetMouseButton(GLFW_MOUSE_BUTTON_LEFT) == GLFW_PRESS && glfwGetKey(GLFW_KEY_LALT)==1)) {
                vec2 p = transform_screen_coords(x, y);
                //We calculate the delta y between the beginning of the right click and the current moment, this gives us a factor of zoom
                float delta = p[1] - previous_y;
                //We create a translation matrix along the z axis
                mat4 zoom_matrix = Eigen::Affine3f(Eigen::Translation3f(vec3(0.0f, 0.0f, delta*5.0f))).matrix();
                //And we aply it to the view matrix.
                view_matrix = zoom_matrix*view_matrix;

                //And we store the current y position
                //(because the user could pause during a zoom -> this case is not covered by the mouse_button callback).
                previous_y =  p[1];
            }
        }
    }
}

///---------BEZIER EDITION---------

bool unproject (int mouse_x, int mouse_y, vec3 &p) {

    mat4 model = (Transform::Identity()).matrix();
    // 1) Compute the inverse of MVP
    mat4 inv_mvp = (projection_matrix*final_view_matrix*model).inverse();

    // 2) Find new screen-space coordinates from mouse position
    vec2 temp = transform_screen_coords(mouse_x,mouse_y);

    //Previous point in screen coordinates
    vec4 previous_pos_view = projection_matrix*final_view_matrix*model*vec4(p[0],p[1],p[2],1.0);
    float factor = previous_pos_view[3] == 0 ? 1.0 : previous_pos_view[3];
    previous_pos_view = previous_pos_view * 1.0f/factor;
    vec4 p_screen = vec4(temp[0],temp[1],previous_pos_view[2],1.0f);

    // 3) Obtain object coordinates p
    vec4 new_pos = inv_mvp*p_screen;
    float factor2 = new_pos[3]==0 ? 1.0 : new_pos[3];
    p = (1.0f / factor2)*vec3(new_pos[0],new_pos[1],new_pos[2]);

    return true;
}

void render_selection() {
    glViewport(0,0,WIDTH,HEIGHT);
    mat4 ident = (Transform::Identity()).matrix();
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
    for (unsigned int i = 0; i < cam_pos_points.size(); i++) {
            cam_pos_points[i].draw_selection(ident, final_view_matrix, projection_matrix);
    }
    for (unsigned int i = 0; i < cam_look_points.size(); i++) {
           cam_look_points[i].draw_selection(ident, final_view_matrix, projection_matrix);
    }
}

void selection_button(int button, int action) {
    static int x_pressed = 0, y_pressed = 0;
    if (button == GLFW_MOUSE_BUTTON_LEFT && action == GLFW_PRESS) {
        //Handle selection
        int x = 0, y = 0;
        glfwGetMousePos(&x, &y);
        x_pressed = x; y_pressed = y;

        render_selection();
        glFlush();
        glFinish();

        unsigned char res[4];
        //reading the color under the cursor, to see on which bezier control point we clicked.
        glReadPixels(x, HEIGHT - y, 1,1,GL_RGBA, GL_UNSIGNED_BYTE, &res);
        selected_point = res[0];
        if (selected_point >= 0 && selected_point < cam_pos_points.size())
            cam_pos_points[selected_point].selected() = true;

        //We take into account the index shift introduced at the beginning
        if (selected_point >= cam_pos_points.size() && selected_point < cam_look_points.size()+cam_pos_points.size())
            cam_look_points[selected_point-cam_pos_points.size()].selected() = true;
    }

    if (button == GLFW_MOUSE_BUTTON_LEFT && action == GLFW_RELEASE) {
        //Handle deselection
        if (selected_point >= 0 && selected_point < cam_pos_points.size()) {
            cam_pos_points[selected_point].selected() = false;
        }

        if (selected_point >= cam_pos_points.size() && selected_point < cam_look_points.size()+cam_pos_points.size()) {
            cam_look_points[selected_point-cam_pos_points.size()].selected() = false;
        }
        int x = 0, y = 0;
        glfwGetMousePos(&x, &y);
        if (x == x_pressed && y == y_pressed) {
            return;
        }

        if (selected_point >= 0 && selected_point < cam_pos_points.size()) {
            unproject(x, y, cam_pos_points[selected_point].position());
            cam_pos_curve.set_points(cam_pos_points);
        }

        if (selected_point >= cam_pos_points.size() && selected_point < cam_look_points.size()+cam_pos_points.size()) {
            unproject(x, y, cam_look_points[selected_point-cam_pos_points.size()].position());
            cam_look_curve.set_points(cam_look_points);
         }
    }
}

///---------UTILITIES-----------

int signum(float value){
   return (value >0.0f)-(value <0.0f);
}

float mix(float from, float to, float alpha){
    return from+alpha*(to-from);
}

// Gets called when the windows is resized.
void resize_callback(int width, int height) {
    WIDTH = width;
    HEIGHT = height;
    glViewport(0, 0, WIDTH, HEIGHT);
    TwWindowSize(WIDTH, HEIGHT);
    projection_matrix = Eigen::perspective(fov, WIDTH / float(HEIGHT), 0.005f,50.0f).matrix();
}

///-----------MISC.--------

#ifdef WITH_ANTTWEAKBAR
//Specific callbacks for Anttweakbar
void TW_CALL regen_noise(void *clientData)
{
    generate_noise();
}

void TW_CALL set_rain(void *clientData){
    if(!rainy){
        particles.stop =false;
    } else {
        particles.stop = true;
    }
    rainy = ! rainy;
}
#endif

void cleanup(){
    glDeleteProgram(_pid_bezier);
    glDeleteProgram(_pid_point);
    glDeleteProgram(_pid_point_selection);
#ifdef WITH_ANTTWEAKBAR
    TwTerminate();
#endif
}

///-----------MAIN------------------------
int main(int, char**){
    glfwInitWindowSize(WIDTH, HEIGHT);
    glfwCreateWindow("ICG Project - 2015 - Group 10");
    glfwDisplayFunc(display);
    glfwSetWindowSizeCallback(&resize_callback);
    glfwSetMouseButtonCallback(mouse_button);
    glfwSetMousePosCallback(mouse_pos);
    glfwSetKeyCallback(keyboard);
    init();
    generate_noise();

    glfwMainLoop();
    cleanup();
    return EXIT_SUCCESS;
}
