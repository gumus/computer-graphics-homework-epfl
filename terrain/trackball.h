#pragma once
#include "icg_common.h"
//Taken from HW3
class Trackball {
public:
    Trackball() : _radius(1.0f) {}

    // This function is called when the user presses the left mouse button down.
    // x, and y are in [-1, 1]. (-1, -1) is the bottom left corner while (1, 1)
    // is the top right corner.
    void begin_drag(float x, float y) {
      _anchor_pos = vec3(x, y, 0.0f);
      project_onto_surface(_anchor_pos);
    }

    // This fucntion is called while the user moves the curser around while the
    // left mouse button is still pressed.
    // x, and y are in [-1, 1]. (-1, -1) is the bottom left corner while (1, 1)
    // is the top right corner.
    // Returns the rotation of the trackball in matrix form.
    mat4 drag(float x, float y) {
        // TODO 3: Calculate the rotation given the projections of the anocher
        // point and the current position. The rotation axis is given by the cross
        // product of the two projected points, and the angle between them can be
        // used as the magnitude of the rotation.
        // You might want to scale the rotation magnitude by a scalar factor.
        // p.s. No need for using complicated quaternions as suggested in the wiki
        // article.

      vec3 current_pos = vec3(x, y, 0.0f);
      project_onto_surface(current_pos);

      // Axis = anchor x current
      vec3 axis = _anchor_pos.cross(current_pos);

      //We then calculate the magnitude knwoing the axis vector

      //NOFIX - standard equation to compute the magnitude between two vectors on a sphere
      //float magnitude = atan(axis.norm()/current_pos.dot(_anchor_pos))*0.8;

      //FIX with magic value - fix the inversion of sign of the magnitude by translating the curve by +2
      float magnitude = atan(axis.norm()/(current_pos.dot(_anchor_pos)+2))*2.0f;

      //std::cout << "Mag: " << current_pos.dot(_anchor_pos) << endl;
      mat4 rotation = mat4::Identity();
      rotation = Eigen::Affine3f(Eigen::AngleAxisf(magnitude, axis.normalized() )).matrix();
      return rotation;
    }

private:
    // Projects the point p (whose z coordiante is still empty/zero) onto the
    // trackball surface. If the position at the mouse cursor is outside the
    // trackball, use a hyberbolic sheet as explained in:
    // https://www.opengl.org/wiki/Object_Mouse_Trackball.
    // The trackball radius is given by '_radius'.
    void project_onto_surface(vec3& p) const {
      // TODO 2: Implement this function. Read above link for details.
        float z;

        if (p.x()*p.x()+p.y()*p.y() <= 0.5 * _radius*_radius){  //Case of the sphere
            z = sqrt(_radius*_radius - p.x()*p.x() - p.y()*p.y());
        } else {    //Case of the hyperbolic sheet
            z = 0.5 * _radius *_radius / sqrt(p.x()*p.x()+p.y()*p.y());
        }
        p.z()=z;
    }

    float _radius;
    vec3 _anchor_pos;
    mat4 _rotation;
};
