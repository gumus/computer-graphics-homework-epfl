#version 330 core
in vec3 position;

uniform mat4 vp;

out float scale;
out float height;

float rand(vec2 pos){
   return fract(sin(dot(pos.xy,vec2(12.9898,78.233))) * 43758.5453);
}

void main(){
    height = position.y;
    gl_Position = vp*vec4(position,1.0);
    scale = gl_Position.z;
    //simulate variation of size due to perpective
    gl_PointSize = min(40.0,1.0/(scale*scale));
    if(height==0){
        //For waves, we randomize the size (we have only one tetxure)
        gl_PointSize = min(100.0,1.0/(scale*scale)) * (1.0+rand(position.xz))*0.5;
    }

}
