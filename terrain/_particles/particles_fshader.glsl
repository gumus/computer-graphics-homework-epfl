#version 330 core
in float height;
in float scale;

uniform sampler2D tex;
uniform sampler2D wave;
uniform vec3 cam_pos;

out vec4 color;

void main(){
    float alpha = 0.0;
    if(height != 0.0){ //Raindrop
         alpha = texture(tex,vec2(gl_PointCoord.x,-gl_PointCoord.y)).r;
    } else { //Wave
        //We define a new uv set to deform the texture (for perspective)
        vec2 new_uv = clamp((gl_PointCoord - vec2(0.0,0.5))*vec2(2.0,1.0/(((1.0-normalize(cam_pos).y)*0.45) - 0.5)),0.0,1.0);
        alpha = texture(wave,new_uv).r;
    }

    if(alpha == 0.0){
        discard;
    }
    color = vec4(1,1,1,alpha);

}
