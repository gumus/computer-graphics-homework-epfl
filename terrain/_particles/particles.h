#pragma once
#include "icg_common.h"

class Particles {
private:
    GLuint _vao;          ///< vertex array object
    GLuint _pid;          ///< GLSL shader program ID
    GLuint _vbo_position;
    GLuint _tex;
    GLuint _wave;
public:

    //Particle struct
    struct Particle{
        vec3 pos, speed;
        float weight;
        bool alive;// to know if we have to update the particle/can reuse it
        float life;//to know if the particle is a raindrop or a wavelet
    };

    static const int MaxParticles = 200000;
    Particle ParticlesContainer[MaxParticles];
    int lastUsedParticle = 0;

    //Rain enabled or not
    bool stop = true;

    void init(){
        // Compile the shaders.
        _pid = opengp::load_shaders("_particles/particles_vshader.glsl", "_particles/particles_fshader.glsl");
        if(!_pid)
          exit(EXIT_FAILURE);
        glUseProgram(_pid);
        //VertexArray
        glGenVertexArrays(1, &_vao);
        glBindVertexArray(_vao);

        //Initalize random seed
        srand (static_cast <unsigned> (glfwGetTime()));

        glGenBuffers(1, &_vbo_position);
        glBindBuffer(GL_ARRAY_BUFFER, _vbo_position);

        //initalisation : all particles are under the terrain, marked as dead (so we won't draw them)
        GLfloat initialPositions[MaxParticles*3];
        for(int i = 0; i < MaxParticles; i++){
            initialPositions[i*3] = 0.0;
            initialPositions[i*3+1] = -3.0;
            initialPositions[i*3+2] = 0.0;
        }

        //Buffer data, we use GL_DYNAMIC_DRAW
        glBufferData(GL_ARRAY_BUFFER, MaxParticles*3*sizeof(GLfloat), initialPositions, GL_DYNAMIC_DRAW);

        //position shader attribute
        GLuint loc_position = glGetAttribLocation(_pid, "position"); ///< Fetch attribute ID for vertex positions
        glEnableVertexAttribArray(loc_position); /// Enable it
        glVertexAttribPointer(loc_position, 3, GL_FLOAT, DONT_NORMALIZE, ZERO_STRIDE, ZERO_BUFFER_OFFSET);

        //Drop texture
        glGenTextures(1, &_tex);
        glBindTexture(GL_TEXTURE_2D, _tex);
        glfwLoadTexture2D("_particles/drop.tga", 0);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
        GLuint tex_id = glGetUniformLocation(_pid, "tex");
        glUniform1i(tex_id, 0);

        //Wave texture
        glGenTextures(1, &_wave);
        glBindTexture(GL_TEXTURE_2D, _wave);
        glfwLoadTexture2D("_particles/atlas.tga", 0);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
        GLuint wave_id = glGetUniformLocation(_pid, "wave");
        glUniform1i(wave_id, 1);


        ///--- to avoid the current object being polluted
        glBindVertexArray(0);
        glUseProgram(0);
    }
       
    void cleanup(){     
        glDeleteTextures(1, &_tex);
        glDeleteTextures(1, &_wave);
        glDeleteBuffers(1, &_vbo_position);
        glDeleteVertexArrays(1, &_vao);
    }

    //Taken from opengl-tutorial, to find unused particles
    int findUnusedParticle(){
        for(int i=lastUsedParticle; i<MaxParticles; i++){
            if (!ParticlesContainer[i].alive){
               lastUsedParticle = i;
               return i;
            }
        }
        for(int i=0; i<lastUsedParticle; i++){
            if (!ParticlesContainer[i].alive){
                lastUsedParticle = i;
                return i;
            }
        }
        return 0; // All particles are taken, override the first one
    }

    void updateParticles(float deltaTime,vec3 camera_position){
        glBindBuffer(GL_ARRAY_BUFFER, _vbo_position);

        //Compute number of particles to create based on time elapsed since last draw call
        int newparticles = (int)(deltaTime*10000.0);
        if (newparticles > (int)(0.022f*10000.0))
            newparticles = (int)(0.022f*10000.0);
        newparticles*=1;

        //Generating new particles
        if(!stop){
            //We generate particles only around the camera
            float mini_x = camera_position[0]-0.4;
            float maxi_x = camera_position[0]+0.4;
            float mini_y = camera_position[1]-0.1;
            float maxi_y = camera_position[1]+0.1;
            float mini_z = camera_position[2]-0.4;
            float maxi_z = camera_position[2]+0.4;

            for(int i = 0; i < newparticles;i++){
                int index = findUnusedParticle();
                //We create a particle at this index, with random position
                Particle& p = ParticlesContainer[index];
                p.alive = true;
                p.life = 0.2;
                //Particle at random position in the defined box
                float x  =  mini_x + static_cast <float> (rand())/(static_cast <float> (RAND_MAX/(maxi_x-mini_x)));
                float y  =  mini_y + static_cast <float> (rand())/(static_cast <float> (RAND_MAX/(maxi_y-mini_y)));
                float z  =  mini_z + static_cast <float> (rand())/(static_cast <float> (RAND_MAX/(maxi_z-mini_z)));
                p.pos = vec3(x,y,z);
                p.speed =vec3(0,0,0);
                p.weight = 0.5;//(0.4 + static_cast <float> (rand()) /( static_cast <float> (RAND_MAX/(0.2))));
            }
        }
        //For each particle, we determine its state, and update it properly if needed
        for(int i = 0; i < MaxParticles; i++){
            Particle &p = ParticlesContainer[i];

                if (p.alive && p.pos.y()>0.0){
                    //p is alive and falling -> real raindrop
                    p.speed = p.speed + deltaTime * vec3(0.0,-9.81,0.0)*p.weight;
                    p.pos = p.pos + deltaTime * p.speed*0.05;
                    float position[3] = {p.pos.x(),p.pos.y(),p.pos.z()};
                    glBufferSubData(GL_ARRAY_BUFFER,3*sizeof(GLfloat)*i,3*sizeof(GLfloat),position);
                }else if(p.pos.y()<=0.0){
                    if(p.alive && p.life >0.0) {
                        //p is alive, but has reached the level of the water ->
                        // if it has non-zero lifetime, it becomes a "splash"
                        p.pos.y()=0.0;
                        p.life -= deltaTime;
                        float position[3] = {p.pos.x(),p.pos.y(),p.pos.z()};
                        glBufferSubData(GL_ARRAY_BUFFER,3*sizeof(GLfloat)*i,3*sizeof(GLfloat),position);
                    } else if(p.alive){
                        //p is alive, but out of lifetime, we kill it and move it under the terrain
                        p.speed = vec3(0,0,0);
                        p.pos = vec3(0,-3,0);
                        p.alive = false;
                        float position[3] = {p.pos.x(),p.pos.y(),p.pos.z()};
                        glBufferSubData(GL_ARRAY_BUFFER,3*sizeof(GLfloat)*i,3*sizeof(GLfloat),position);
                    }
                }
                //else, p is dead, we dont update it
        }
        check_error_gl();
    }

    void draw(mat4 view, mat4 projection,vec3 camera_position, float deltaTime){

        glUseProgram(_pid);
        glBindVertexArray(_vao);
        glBindBuffer(GL_ARRAY_BUFFER, _vbo_position);

        //Updating the particle buffer
        updateParticles(deltaTime,camera_position);

        glActiveTexture(GL_TEXTURE0);
        glBindTexture(GL_TEXTURE_2D, _tex);
        glActiveTexture(GL_TEXTURE1);
        glBindTexture(GL_TEXTURE_2D, _wave);

        //No need for a model matrix here
        mat4 vp = projection * view;
        GLuint vp_id = glGetUniformLocation(_pid, "vp");
        glUniformMatrix4fv(vp_id, 1, GL_FALSE, vp.data());
        glUniform3fv(glGetUniformLocation(_pid, "cam_pos"),ONE, camera_position.data());

        //We need to set the draw mode to points, enabling alpha
        glEnable( GL_BLEND );
        glEnable(GL_PROGRAM_POINT_SIZE);
        glDrawArrays(GL_POINTS, 0, MaxParticles);
        glDisable(GL_PROGRAM_POINT_SIZE);
        glDisable( GL_BLEND );
        glBindBuffer(GL_ARRAY_BUFFER, 0);
        check_error_gl();
        glBindVertexArray(0);
        glUseProgram(0);
    }
};
