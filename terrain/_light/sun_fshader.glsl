#version 330 core
//NOT USED ANYMORE
uniform sampler2D tex;

out vec4 color;
in vec2 uv;
uniform vec3 light_pos;

void main(){


    //We compute the distance from the current point to the center of the quad
    vec2 coords = (uv - vec2(0.5))*2.0;
    float radius = sqrt(coords.x*coords.x+coords.y*coords.y);
    //We read the alpha value from a texture where x = radius and y=height in the sky
    float time = clamp(normalize(light_pos).y,0.01,1);


    vec3 light_color = texture(tex,vec2(radius,time)).rgb;//vec3(0.98,1.0,0.99);
    //We need alpha activated before drawing
    //vec3 light_color = mix(vec3(0.98,1.0,0.99),vec3(0.99,0.54,0.09),vec3((1-time)*0.5));
    float alpha = 0;

     color = vec4(light_color,alpha);
    if(radius < 1.0-0.001){//< we need a small bias to avoid flickering. Choose it <1/image_size.
        color = texture(tex,vec2(radius,time)).rgba;
    }


   // color = vec4(pow(color.r, 1.0 / 2.2),pow(color.g, 1.0 / 2.2),pow(color.b, 1.0 / 2.2),color.a);

}
