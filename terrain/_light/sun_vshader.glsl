#version 330 core
//NOT USED ANYMORE
in vec2 vtexcoord;
in vec3 position;

uniform mat4 mvp;

out vec2 uv;

void main(){
    gl_Position = mvp * vec4(position, 1.0);
    uv = vtexcoord;
}
