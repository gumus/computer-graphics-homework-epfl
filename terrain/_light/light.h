#pragma once
#include "icg_common.h"

class Light {
private:
    GLuint _vao;          ///< vertex array object
    GLuint _vbo_position; ///< memory buffer for positions
    GLuint _vbo_index;    ///< memory buffer for indices
    GLuint _pid;          ///< GLSL shader program ID
    GLuint _num_indices;  ///< number of vertices to render
    GLuint _tex;
    GLuint _tex1;
    GLuint _vbo_vtexcoord;

public:

    vec3 light_pos =  vec3(1.0f, 1.0f, 1.0f);

    //Normalized altitude of the sun
    float getSunAltitude(){
            return light_pos.normalized()[1];
    }

    //Returns the color of the light based on the sun position
    vec3 getLightColor(){
        //non-linear gradient beewteen white and orange-yellow
        vec3 light_color = vec3(1,1,1)+(1-pow(abs(getSunAltitude()),0.28))*(vec3(1.0,0.85,0.58)-vec3(1,1,1));
        if(getSunAltitude()<0.04){
            float mix_factor = abs(fmin(0,getSunAltitude()))*2.0;
            light_color = light_color + mix_factor*(vec3(0.0,0.0,0.0)-light_color);
        }
        return light_color;
    }

    //Returns the color of the water (depends on the time of the day)
    vec3 getDeepColor(){
        return vec3(0.05,0.25,0.45)*max((float)0.0,getSunAltitude());
    }


    //We don't display the sun as a quad anymore
    void init(){
        // Compile the shaders.
        /*_pid = opengp::load_shaders("_light/sun_vshader.glsl", "_light/sun_fshader.glsl");
        if(!_pid)
          exit(EXIT_FAILURE);
        glUseProgram(_pid);

        // Vertex one vertex array
        glGenVertexArrays(1, &_vao);
        glBindVertexArray(_vao);

        // Position buffer
        const GLfloat position[] = {-1.0f, -1.0f,  0.0f,  // left, bottom, front
                                     1.0f, -1.0f,  0.0f,  // right, bottom, front
                                     1.0f,  1.0f,  0.0f,  // right, top, front
                                    -1.0f,  1.0f,  0.0f,  // left, top, front
                                    }; // left, top, back

        glGenBuffers(1, &_vbo_position);
        glBindBuffer(GL_ARRAY_BUFFER, _vbo_position);
        glBufferData(GL_ARRAY_BUFFER, sizeof(position), position, GL_STATIC_DRAW);

        // position shader attribute
        GLuint loc_position = glGetAttribLocation(_pid, "position"); ///< Fetch attribute ID for vertex positions
        glEnableVertexAttribArray(loc_position); /// Enable it
        glVertexAttribPointer(loc_position, 3, GL_FLOAT, DONT_NORMALIZE, ZERO_STRIDE, ZERO_BUFFER_OFFSET);

        // Index buffer
        const GLuint index[] = {0, 2, 1,  // Front face triangle 1
                                0, 3, 2,  // Front face triangle 2
                                };

        _num_indices = sizeof(index) / sizeof(GLuint);

        glGenBuffers(1, &_vbo_index);
        glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, _vbo_index);
        glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(index), index, GL_STATIC_DRAW);
    
        ///--- Texture coordinates
                {
                    const GLfloat vtexcoord[] = {    0.0f, 0.0f,
                                                     1.0f, 0.0f,
                                                     1.0f, 1.0f,
                                                     0.0f, 1.0f};

                    ///--- Buffer
                    glGenBuffers(1, &_vbo_vtexcoord);
                    glBindBuffer(GL_ARRAY_BUFFER, _vbo_vtexcoord);
                    glBufferData(GL_ARRAY_BUFFER, sizeof(vtexcoord), vtexcoord, GL_STATIC_DRAW);

                    ///--- Attribute
                    GLuint vtexcoord_id = glGetAttribLocation(_pid, "vtexcoord");
                    glEnableVertexAttribArray(vtexcoord_id);
                    glVertexAttribPointer(vtexcoord_id, 2, GL_FLOAT, DONT_NORMALIZE, ZERO_STRIDE, ZERO_BUFFER_OFFSET);
                }
        // Load texture from disk

                glGenTextures(1, &_tex);
                glBindTexture(GL_TEXTURE_2D, _tex);
                glfwLoadTexture2D("_light/sun.tga", 0);
                glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
                glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);

                // Texture uniforms
                GLuint tex_id = glGetUniformLocation(_pid, "tex");
                glUniform1i(tex_id, 0);

        // to avoid the current object being polluted
        glBindVertexArray(0);
        */
    }
       
    void cleanup(){     
        glDeleteBuffers(1, &_vbo_position);
        glDeleteBuffers(1, &_vbo_index);
        glDeleteTextures(1, &_tex);
        glDeleteVertexArrays(1, &_vao);
    }

    //We don't display the sun as a quad anymore
    void draw(const mat4& model, const mat4& view, const mat4& projection, bool keep_translation = false){
        glUseProgram(_pid);
        glBindVertexArray(_vao);
        glActiveTexture(GL_TEXTURE0);
        glBindTexture(GL_TEXTURE_2D, _tex);

        glActiveTexture(GL_TEXTURE1);
        glBindTexture(GL_TEXTURE_2D, _tex1);


        glUniform3fv(glGetUniformLocation(_pid, "light_pos"), ONE, light_pos.data());


        mat4 view_final = mat4::Zero();
       if (!keep_translation){

           view_final.block(0,0,3,3) = view.block(0,0,3,3);
           view_final(3,3) = 1.0;

       }else {
           view_final = view;
       }
            // Setup MVP
            mat4 MVP = projection*view_final*model;
            GLuint MVP_id = glGetUniformLocation(_pid, "mvp");
            glUniformMatrix4fv(MVP_id, 1, GL_FALSE, MVP.data());

            //Draw
            glDrawElements(GL_TRIANGLES, _num_indices, GL_UNSIGNED_INT, 0);
        glBindVertexArray(0);
        glUseProgram(0);
    }
};
