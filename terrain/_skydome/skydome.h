#pragma once
#include "icg_common.h"
#include "../_light/light.h"
#undef M_PI
#define M_PI 3.14159

typedef Eigen::Transform<float,3,Eigen::Affine> Transform;



class Skydome {

protected:
    GLuint _vao; ///< vertex array object
    GLuint _pid;
    opengp::Surface_mesh mesh;    
    GLuint _vpoint;    ///< memory buffer
    GLuint _vnormal;   ///< memory buffer
    GLuint _tint;
    GLuint _sun;
    GLuint _moon;
    GLuint _clouds;
    GLuint _clouds2;

public:
    float height = M_PI/4.0;
    void init(){
        //The skydome is a sphere, laoded from an .obj
        assert(mesh.read("_skydome/sphere.obj"));
        mesh.triangulate();
        mesh.update_vertex_normals();

        _pid = opengp::load_shaders("_skydome/skydome_vshader.glsl", "_skydome/skydome_fshader.glsl");
        if(!_pid) exit(EXIT_FAILURE);
        glUseProgram(_pid);

        ///--- Vertex one vertex Array
        glGenVertexArrays(1, &_vao);
        glBindVertexArray(_vao);
        check_error_gl();        
        
        ///--- Vertex Buffer
        Surface_mesh::Vertex_property<Point> vpoints = mesh.get_vertex_property<Point>("v:point");
        glGenBuffers(ONE, &_vpoint);
        glBindBuffer(GL_ARRAY_BUFFER, _vpoint);
        glBufferData(GL_ARRAY_BUFFER, mesh.n_vertices() * sizeof(vec3), vpoints.data(), GL_STATIC_DRAW);
        check_error_gl();        
    
        ///--- Normal Buffer
        Surface_mesh::Vertex_property<Normal> vnormals = mesh.get_vertex_property<Normal>("v:normal");
        glGenBuffers(ONE, &_vnormal);
        glBindBuffer(GL_ARRAY_BUFFER, _vnormal);
        glBufferData(GL_ARRAY_BUFFER, mesh.n_vertices() * sizeof(vec3), vnormals.data(), GL_STATIC_DRAW);
        check_error_gl();        
    
        ///--- Index Buffer
        std::vector<unsigned int> indices;
        for(Surface_mesh::Face_iterator fit = mesh.faces_begin(); fit != mesh.faces_end(); ++fit) {
            unsigned int n = mesh.valence(*fit);
            Surface_mesh::Vertex_around_face_circulator vit = mesh.vertices(*fit);
            for(unsigned int v = 0; v < n; ++v) {
                indices.push_back((*vit).idx());
                ++vit;
            }
        }

        GLuint _vbo_indices;
        glGenBuffers(ONE, &_vbo_indices);
        glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, _vbo_indices);
        glBufferData(GL_ELEMENT_ARRAY_BUFFER, indices.size() * sizeof(unsigned int), &indices[0], GL_STATIC_DRAW);
        check_error_gl();        

        glBindVertexArray(_vao);
        check_error_gl();

        ///--- Vertex Attribute ID for Positions
        GLint vpoint_id = glGetAttribLocation(_pid, "vpoint");
        if (vpoint_id >= 0) {
            glEnableVertexAttribArray(vpoint_id);
            check_error_gl();
            glBindBuffer(GL_ARRAY_BUFFER, _vpoint);
            glVertexAttribPointer(vpoint_id, 3 /*vec3*/, GL_FLOAT, DONT_NORMALIZE, ZERO_STRIDE, ZERO_BUFFER_OFFSET);
            check_error_gl();
        }

        ///--- Vertex Attribute ID for Normals
        GLint vnormal_id = glGetAttribLocation(_pid, "vnormal");
        if (vnormal_id >= 0) {
            glEnableVertexAttribArray(vnormal_id);
            glBindBuffer(GL_ARRAY_BUFFER, _vnormal);
            glVertexAttribPointer(vnormal_id, 3 /*vec3*/, GL_FLOAT, DONT_NORMALIZE, ZERO_STRIDE, ZERO_BUFFER_OFFSET);
            check_error_gl();
        }

        // Load textures from disk

        // The tint is the general color of the sky.
        //Horizontal axis : time of the day (=height of the sun in the sky)
        //Vertical axis : height of the current fragment in the scene
        glGenTextures(1, &_tint);
        glBindTexture(GL_TEXTURE_2D, _tint);
        glfwLoadTexture2D("_skydome/tint.tga", 0);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
        // Texture uniforms
        GLuint tint_id = glGetUniformLocation(_pid, "tint");
        glUniform1i(tint_id, 0);
        check_error_gl();

        //Sun texture : determine the color and size of the sun based on its position (vertical axis)
        glGenTextures(1, &_sun);
        glBindTexture(GL_TEXTURE_2D, _sun);
        glfwLoadTexture2D("_skydome/sun.tga", 0);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
        // Texture uniforms
        GLuint glow_id = glGetUniformLocation(_pid, "sun");
        glUniform1i(glow_id, 1);
        check_error_gl();

        //Spherical projection of clouds, generated with Vue
        glGenTextures(1, &_clouds);
        glBindTexture(GL_TEXTURE_2D, _clouds);
        glfwLoadTexture2D("_skydome/clouds1.tga", 0);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
        // Texture uniforms
        GLuint cloud_id = glGetUniformLocation(_pid, "clouds1");
        glUniform1i(cloud_id, 2);
        check_error_gl();

        //Spherical projection of rainy clouds, generated with Vue,
        glGenTextures(1, &_clouds2);
        glBindTexture(GL_TEXTURE_2D, _clouds2);
        glfwLoadTexture2D("_skydome/clouds2.tga", 0);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
        // Texture uniforms
        GLuint cloud_id2 = glGetUniformLocation(_pid, "clouds2");
        glUniform1i(cloud_id2, 3);
        check_error_gl();

        //Moon texture
        glGenTextures(1, &_moon);
        glBindTexture(GL_TEXTURE_2D, _moon);
        glfwLoadTexture2D("_skydome/moon.tga", 0);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
        // Texture uniforms
        GLuint moon_id = glGetUniformLocation(_pid, "moon");
        glUniform1i(moon_id, 4);
        check_error_gl();

        ///--- Unbind
        glBindVertexArray(0);
        glUseProgram(0);

    }
           
    void cleanup(){
        glDeleteTextures(1, &_sun);
        glDeleteTextures(1, &_tint);
        glDeleteTextures(1, &_moon);
        glDeleteTextures(1, &_clouds);
        glDeleteTextures(1, &_clouds2);
    }


    void draw(const mat4& model, const mat4& view, const mat4& projection, Light& light, float weather,bool keep_translation = false){
        glUseProgram(_pid);
        glBindVertexArray(_vao);

        // Bind textures
        glActiveTexture(GL_TEXTURE0);
        glBindTexture(GL_TEXTURE_2D, _tint);
        glActiveTexture(GL_TEXTURE1);
        glBindTexture(GL_TEXTURE_2D, _sun);
        glActiveTexture(GL_TEXTURE2);
        glBindTexture(GL_TEXTURE_2D, _clouds);
        glActiveTexture(GL_TEXTURE3);
        glBindTexture(GL_TEXTURE_2D, _clouds2);
        glActiveTexture(GL_TEXTURE4);
        glBindTexture(GL_TEXTURE_2D, _moon);

        //If we want the skydome to follow the camera, we can use the view without its translation part
        mat4 view_without_translation = mat4::Zero();
        view_without_translation.block(0,0,3,3) = view.block(0,0,3,3);
        view_without_translation(3,3) = view(3,3);
        if(keep_translation){
            view_without_translation = view;
        }

        //MVP matrix
        mat4 mvp = projection*view_without_translation*model;
        glUniformMatrix4fv(glGetUniformLocation(_pid, "mvp"), ONE, DONT_TRANSPOSE, mvp.data());

        //Rotation matrix for the stars
        float angle = atan2(light.light_pos[2],light.light_pos[0]);
        mat3 rotation = (Eigen::AngleAxisf(angle*0.6, vec3(0,1,0))*Eigen::AngleAxisf(0.55, vec3(-1,0,0))*Transform::Identity()).matrix().block<3,3>(0,0);
        glUniformMatrix3fv(glGetUniformLocation(_pid, "rot_stars"), ONE, DONT_TRANSPOSE, rotation.data());

        //Other uniforms
        glUniform1f(glGetUniformLocation(_pid, "time"),glfwGetTime()*0.001);
        glUniform1f(glGetUniformLocation(_pid, "weather"),weather);
        glUniform3fv(glGetUniformLocation(_pid, "sun_pos"),1,light.light_pos.data());

        glDrawElements(GL_TRIANGLES, /*#vertices*/ 3*mesh.n_faces(), GL_UNSIGNED_INT, ZERO_BUFFER_OFFSET);

        glBindVertexArray(0);
        glUseProgram(0);
    }
};
