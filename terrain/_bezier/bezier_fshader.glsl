#version 330 core

uniform int id;

out vec3 color;

void main() {
    if(id==1){
        color = vec3(0,0,1);
    } else {
        color = vec3(1,0,0);
    }
}
