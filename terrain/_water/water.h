#pragma once
#include "icg_common.h"
#include "../_light/light.h"



class Water  {
protected:
    GLuint _vao;          ///< vertex array object
    GLuint _vbo_position; ///< memory buffer for positions
    GLuint _vbo_index;    ///< memory buffer for indice
    GLuint _pid;          ///< GLSL shader program ID
    GLuint _tex;          ///< Texture ID
    GLuint _vbo;
    GLuint _num_indices;  ///< number of vertices to render

    GLuint _tex_wo_water;
    GLuint _tex_under_water;
    GLuint _height_map;
    GLuint _normal_map_1;
    GLuint _normal_map_2;
    GLuint _shadow_map;
    mat4 identity;
    
public:
    float scale_factor;
    float bias = 0.01;

    void init(GLuint scene_wo_water, GLuint scene_under_water, GLuint height_map,GLuint shadow_map){
        // Compile the shaders
        _pid = opengp::load_shaders("_water/water_vshader.glsl", "_water/water_fshader.glsl");
        if(!_pid) exit(EXIT_FAILURE);       
        glUseProgram(_pid);
        
        // Vertex one vertex Array
        glGenVertexArrays(1, &_vao);
        glBindVertexArray(_vao);
        check_error_gl();
        // Vertex coordinates and indices

        ///--- Vertex coordinates
            {
                   const GLfloat vpoint[] = { /*V1*/ -1.0f, 0.0f,-1.0f,
                                              /*V2*/ +1.0f, 0.0f,-1.0f,
                                              /*V3*/ -1.0f,  0.0f,+1.0f,
                                              /*V4*/ +1.0f, 0.0f,+1.0f };
                   ///--- Buffer
                   glGenBuffers(1, &_vbo);
                   glBindBuffer(GL_ARRAY_BUFFER, _vbo);
                   glBufferData(GL_ARRAY_BUFFER, sizeof(vpoint), vpoint, GL_STATIC_DRAW);

                   ///--- Attribute
                   GLuint vpoint_id = glGetAttribLocation(_pid, "position");
                   glEnableVertexAttribArray(vpoint_id);
                   glVertexAttribPointer(vpoint_id, 3, GL_FLOAT, DONT_NORMALIZE, ZERO_STRIDE, ZERO_BUFFER_OFFSET);
               }

               ///--- Texture coordinates
               {
                   const GLfloat vtexcoord[] = { /*V1*/ 0.0f, 0.0f,
                                                 /*V2*/ 1.0f, 0.0f,
                                                 /*V3*/ 0.0f, 1.0f,
                                                 /*V4*/ 1.0f, 1.0f};
                   ///--- Buffer
                   glGenBuffers(1, &_vbo);
                   glBindBuffer(GL_ARRAY_BUFFER, _vbo);
                   glBufferData(GL_ARRAY_BUFFER, sizeof(vtexcoord), vtexcoord, GL_STATIC_DRAW);

                   ///--- Attribute
                   GLuint vtexcoord_id = glGetAttribLocation(_pid, "vtexcoord");
                   glEnableVertexAttribArray(vtexcoord_id);
                   glVertexAttribPointer(vtexcoord_id, 2, GL_FLOAT, DONT_NORMALIZE, ZERO_STRIDE, ZERO_BUFFER_OFFSET);
               }

         //Load textures from memory (those coming from framebuffers)
        this->_tex_under_water = scene_under_water;
        glBindTexture(GL_TEXTURE_2D, _tex_under_water);
        glUniform1i(glGetUniformLocation(_pid, "under_water_tex"), 0 /*texture0*/);

        this->_tex_wo_water = scene_wo_water;
        glBindTexture(GL_TEXTURE_2D, _tex_wo_water);
        glUniform1i(glGetUniformLocation(_pid, "wo_water_tex"), 1 /*texture1*/);

        this->_height_map = height_map;
        glBindTexture(GL_TEXTURE_2D, _height_map);
        glUniform1i(glGetUniformLocation(_pid, "height_map"), 2 /*texture2*/);

        // Load normal map textures from disk
        glGenTextures(1, &_normal_map_1);
        glBindTexture(GL_TEXTURE_2D, _normal_map_1);
        glfwLoadTexture2D("_water/normal_map_2.tga", 0);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S,  GL_REPEAT);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T,  GL_REPEAT);
        // Texture uniforms
        glUniform1i(glGetUniformLocation(_pid, "normal_map_1"), 3 /*texture 3*/);

        glGenTextures(1, &_normal_map_2);
        glBindTexture(GL_TEXTURE_2D, _normal_map_2);
        glfwLoadTexture2D("_water/normal_map_1.tga", 0);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S,  GL_REPEAT);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T,  GL_REPEAT);
        // Texture uniforms
        glUniform1i(glGetUniformLocation(_pid, "normal_map_2"), 4 /*texture 4*/);

        //SHADOW MAP TEXTURE 5
        this->_shadow_map = shadow_map;
        glBindTexture(GL_TEXTURE_2D, _shadow_map);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);

        GLuint sha_id = glGetUniformLocation(_pid, "shadow_map");
        glUniform1i(sha_id, 5 /*GL_TEXTURE5*/);

        identity << 1.0f, 0.0f, 0.0f, 0.0f,
                                 0.0f, 1.0f, 0.0f, 0.0f,
                                 0.0f, 0.0f, 1.0f, 0.0f,
                                 0.0f, 0.0f, 0.0f, 1.0f;


        //to avoid the current object being polluted
        glBindVertexArray(0);
        glUseProgram(0);

    }
           
    void cleanup(){
        glDeleteBuffers(1, &_vbo_position);
        glDeleteBuffers(1, &_vbo_index);
        glDeleteVertexArrays(1, &_vao);
        glDeleteProgram(_pid);
        glDeleteTextures(1, &_tex);
        glDeleteTextures(1, &_tex_under_water);
        glDeleteTextures(1, &_tex_wo_water);
        glDeleteTextures(1, &_shadow_map);

    }
    

    void draw(const mat4& model, const mat4& view, const mat4& projection, Light& light, float view_angle_cos,mat4 depth_vp_offset){
        glUseProgram(_pid);
        glBindVertexArray(_vao);

        //Bind textures
        glActiveTexture(GL_TEXTURE0);
        glBindTexture(GL_TEXTURE_2D, _tex_under_water);
        check_error_gl();

        glActiveTexture(GL_TEXTURE1);
        glBindTexture(GL_TEXTURE_2D, _tex_wo_water);
        check_error_gl();

        glActiveTexture(GL_TEXTURE2);
        glBindTexture(GL_TEXTURE_2D, _height_map);
        check_error_gl();

        glActiveTexture(GL_TEXTURE3);
        glBindTexture(GL_TEXTURE_2D, _normal_map_1);
        check_error_gl();

        glActiveTexture(GL_TEXTURE4);
        glBindTexture(GL_TEXTURE_2D, _normal_map_2);
        check_error_gl();

        glActiveTexture(GL_TEXTURE5);
        glBindTexture(GL_TEXTURE_2D, _shadow_map);

        // Setup MVP
        mat4 MV = view*model;
        GLuint MV_id = glGetUniformLocation(_pid, "MV");
        glUniformMatrix4fv(MV_id, 1, GL_FALSE, MV.data());

        //Setup view angle
        glUniform1f(glGetUniformLocation(_pid, "view_angle_cos"),view_angle_cos);
        //Scale factor
        glUniform1f(glGetUniformLocation(_pid, "scale_factor"),scale_factor);
        //Time
        glUniform1f(glGetUniformLocation(_pid, "time"),glfwGetTime());

        //Model
        mat4 modelM = model;
        GLuint modelM_id = glGetUniformLocation(_pid, "model");
        glUniformMatrix4fv(modelM_id, 1, GL_FALSE, modelM.data());

        //View
        GLuint view_id = glGetUniformLocation(_pid, "view");
        glUniformMatrix4fv(view_id, 1, GL_FALSE, view.data());

        //Projection
        mat4 proj = projection;
        GLuint P_id = glGetUniformLocation(_pid, "projection");
        glUniformMatrix4fv(P_id, 1, GL_FALSE, proj.data());

        //Transpose inverse
        mat3 ti_MV = MV.block(0,0,3,3).inverse().transpose();
        GLuint ti_MV_id = glGetUniformLocation(_pid, "inverseTransposeMV");
        glUniformMatrix3fv(ti_MV_id, 1, GL_FALSE, ti_MV.data());

        //Light setup
        vec4 light_vec_1 = (view.inverse().transpose()*vec4(light.light_pos[0],light.light_pos[1],light.light_pos[2],0.0f));
        vec3 light_vec = vec3(light_vec_1[0],light_vec_1[1],light_vec_1[2]);
        glUniform3fv(glGetUniformLocation(_pid, "light_vec"), ONE, light_vec.data());
        glUniform3fv(glGetUniformLocation(_pid, "light_pos"), ONE, light.light_pos.data());
        glUniform3fv(glGetUniformLocation(_pid, "light_color"), ONE, light.getLightColor().data());


        //Shadow map uniforms
        bool test = (depth_vp_offset != identity);
        glUniform1i(glGetUniformLocation(_pid,"shadow_enabled"),test);
        glUniformMatrix4fv(glGetUniformLocation(_pid, "depth_vp_offset"), 1, GL_FALSE, depth_vp_offset.data());
        glUniform1f(glGetUniformLocation(_pid,"bias"),bias);

        glDrawArrays(GL_TRIANGLE_STRIP, 0, 4);

        glBindVertexArray(0);        
        glUseProgram(0);
    }
};
