#version 330 core
in vec2 uv;
in vec3 view_dir;
in vec4 shadow_coord;

uniform sampler2D under_water_tex;
uniform sampler2D wo_water_tex;
uniform sampler2D normal_map_1;
uniform sampler2D normal_map_2;
uniform vec3 light_pos;//<world space
uniform vec3 light_vec;//<camera space
uniform vec3 light_color;
uniform float view_angle_cos;
uniform mat3 inverseTransposeMV;
uniform mat4 projection;
uniform mat4 MV;
uniform mat4 view;
uniform sampler2D height_map;
uniform float time;
uniform float scale_factor;
uniform sampler2D shadow_map;
uniform float bias;
uniform bool shadow_enabled;

// Poisson disk sample locations.
const vec2 poisson_disk[16] = vec2[](
   vec2(-0.94201624, -0.39906216),
   vec2(0.94558609, -0.76890725),
   vec2(-0.094184101, -0.92938870),
   vec2(0.34495938, 0.29387760),
   vec2(-0.91588581, 0.45771432),
   vec2(-0.81544232, -0.87912464),
   vec2(-0.38277543, 0.27676845),
   vec2(0.97484398, 0.75648379),
   vec2(0.44323325, -0.97511554),
   vec2(0.53742981, -0.47373420),
   vec2(-0.26496911, -0.41893023),
   vec2(0.79197514, 0.19090188),
   vec2(-0.24188840, 0.99706507),
   vec2(-0.81409955, 0.91437590),
   vec2(0.19984126, 0.78641367),
   vec2(0.14383161, -0.14100790)
);

out vec3 color;

void main() {
    vec2 local_uv = (uv*18.0 - vec2(9.0))+vec2(0.5);
    //Reading the height for the area where the terrain is
    float height = texture(height_map,vec2(local_uv.x, 1-local_uv.y)).r *scale_factor;

    //Default color
    color = vec3(0.38,0.73,1.0);

    //Normal maps coordinates, three different versions with rotations, scales, speeds
    vec2 uv_n1 = 12*(local_uv*0.5+0.8660*vec2(-local_uv.y,local_uv.x)) + vec2(0.0,time*0.015);
    vec2 uv_n2 = 18*(local_uv*0.5737-0.8191*vec2(-local_uv.y,local_uv.x)) + vec2(0.0,time*0.02) + vec2(0.5,0.2);
    vec2 uv_n3 = 8*(-local_uv) + vec2(0.0,time*0.05) + vec2(0.1,-0.4);

    //Reading and mixing normals
    vec3 normal = mix(texture(normal_map_2,uv_n1).rgb,texture(normal_map_2,uv_n2).rgb,vec3(0.5));
    normal = mix(texture(normal_map_2,uv_n3).rgb,normal,vec3(0.6));
    normal = normalize((normal.xzy-vec3(0.5))*2.0f);
    vec3 n = inverseTransposeMV * normal;
    n = normalize(n);

    // Query window_width/height
    vec2 dimension = textureSize(under_water_tex,0);

    //Computing an offset for waves reflection
    //Using the projection of the world normal onto the water plane
    vec3 flat_normal = normal - dot(normal,vec3(0,1,0))*vec3(0,1,0);
    vec3 flat_view_normal = inverseTransposeMV * flat_normal;
    vec4 flat_proj_normal = (projection*vec4(flat_view_normal/(length(view_dir)*length(view_dir)),0.0));
    vec2 offset = flat_proj_normal.xy * 0.05;

    //This gives us controlled offsets to lookup in the rendered texture coming from the framebuffer
    float x1 = (gl_FragCoord.x /dimension.x) + min(0.07,offset.x);
    float y1 = (gl_FragCoord.y/dimension.y) + min(0.07,offset.y);

    //Mixing reflection and transmitted color, using the angle of view
    color = mix(texture(wo_water_tex,vec2(x1,y1)).rgb, texture(under_water_tex,vec2(x1,1-y1)).rgb, vec3(0.6*view_angle_cos*view_angle_cos*view_angle_cos*view_angle_cos));

    //Lighting
    vec3 l = normalize(light_vec);

    float intensity = max(0,dot(n,l));
    vec3 diffuse = color * max(0.8,intensity)*light_color;
    vec3 specular = vec3(0.0);
    if(intensity > 0.0) {
        vec3 v = normalize(view_dir);
        vec3 R = reflect(-l,n);
        R = normalize(R);
        specular = vec3(0.3) * pow(max(0,dot(R,v)),35);//vec3(0.2)*pow(abs(dot(R,v)), 5);

    }
    color = (diffuse + specular);

    //Border waves
    //Near the coast, we generate foam using a cosine function moving in time, with a bit of smoothing
    if (height > -0.01 && height < 0 && local_uv.x < 1 && local_uv.x > 0 && local_uv.y < 1 && local_uv.y > 0){
        if(cos(15*height/0.01 + 5*time)>0){
                color = mix(color,vec3((light_pos.y+1.0)*0.5),cos(15*height/0.01 + 5*time)*0.9*(1-exp(-(height+0.01)*(height+0.01)))/(1.2*0.00009995));
        }
    }

    //Shadow map
    float shadow = 1.0;  // shading factor from the shadow (1.0 = no shadow, 0.0 = all dark)
     if(shadow_enabled){
       if(shadow_coord.x<1.0 && shadow_coord.y <1.0 && shadow_coord.x >0.0 && shadow_coord.y > 0.0){//<restriction to avoid weird effects at the far cliping plane
            float col = texture(shadow_map,shadow_coord.xy).r;
            float spread = 150.0;
            for (int i=0;i<16;i++){
                if ( texture( shadow_map, shadow_coord.xy + poisson_disk[i]/spread).r  <  shadow_coord.z - bias ){
                    shadow -= (1.0/30.0);
                }
            }
        }
    }

    color = shadow * color;

}


