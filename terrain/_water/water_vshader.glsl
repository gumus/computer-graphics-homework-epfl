#version 330 core
in vec3 position;
in vec2 vtexcoord;

uniform mat4 MV;
uniform mat4 projection;
uniform mat4 model;
uniform mat4 depth_vp_offset;

out vec3 view_dir;
out vec2 uv;
out vec4 shadow_coord;

void main() {
    
    //Computing position
    vec4 vpoint_mv = MV * vec4(position, 1.0);
    gl_Position = projection * vpoint_mv;
    //Just need eye vector here
    view_dir = - vpoint_mv.xyz;
    uv = vtexcoord;

    //Computing shadow map coordinates
    shadow_coord = depth_vp_offset * model * vec4(position,1.0);

}
