#version 330 core
in vec4 vpoint_mv;
in vec3 normal;
in vec2 uv;

uniform mat3 inv_trans_mv;
uniform vec3 light_vec;
uniform sampler2D tex;
uniform vec3 light_color;

out vec3 color;

void main(){

    color = texture(tex,uv).rgb;

    //Compute normal in camera space at each fragment
    vec3 normal_mv = normalize(inv_trans_mv * normal);
    vec3 view_dir = normalize(- vpoint_mv.xyz);


    //Lighting
    float intensity = max(0.0,dot(normal_mv, normalize(light_vec)));
    vec3 diffuse = max(0.35,intensity)*color*light_color;
    vec3 specular = vec3(0.0);
    if(intensity > 0.0) {
        vec3 R = reflect(normalize(light_vec),normal_mv);
        R = normalize(R);
        specular = vec3(0.3) * pow(max(0,dot(R,view_dir)),30);
    }

    //Instead of doing ambient + diffuse +specular (which might be too bright), you do max(ambient, diffuse)+specular basically
    color = diffuse+specular;
}
