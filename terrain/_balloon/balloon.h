#pragma once
#include "icg_common.h"
#include "../_light/light.h"
#undef M_PI
#define M_PI 3.14159

typedef Eigen::Transform<float,3,Eigen::Affine> Transform;

class Balloon {

protected:
    GLuint _vao; ///< vertex array object
    GLuint _pid;
    opengp::Surface_mesh mesh;    
    GLuint _vpoint;    ///< memory buffer
    GLuint _vnormal;   ///< memory buffer
    GLuint _vtex;       ///< memory buffer
    GLuint _tex;

public:

    void init(){
        //Reading the mesh
        assert(mesh.read("_balloon/balloon.obj"));
        mesh.triangulate();//<normally it is already the case, but who knows
        mesh.update_vertex_normals();

        _pid = opengp::load_shaders("_balloon/balloon_vshader.glsl", "_balloon/balloon_fshader.glsl");
        if(!_pid) exit(EXIT_FAILURE);
        glUseProgram(_pid);

        ///--- Vertex one vertex Array
        glGenVertexArrays(1, &_vao);
        glBindVertexArray(_vao);
        check_error_gl();

        int i =0;
        std::vector<vec3> vert_coords;
        std::vector<vec3> normal_coords;
        std::vector<vec2> tex_coords;
        std::vector<unsigned int> indices;

        //We iterate over the faces of the mesh to generate vertex coordinates, normals and textures coordinates.
        //We need to duplicate vertices because each vertex has multiple uv coords.
        for (Surface_mesh::Face_iterator f_it = mesh.faces_begin (); f_it != mesh.faces_end (); ++f_it)
          {
            for (Surface_mesh::Halfedge_around_face_circulator h_it = Surface_mesh::Halfedge_around_face_circulator (&mesh, *f_it).begin ();
                 h_it != Surface_mesh::Halfedge_around_face_circulator (&mesh, *f_it).end (); ++h_it)
            {
              Surface_mesh::Vertex v = mesh.to_vertex(*h_it);
              vert_coords.push_back(mesh.position(v));
              indices.push_back(i);
              i++;
              Vec3f t = mesh.get_halfedge_property<Vec3f> ("h:texcoord") [*h_it];
              tex_coords.push_back (vec2(t[0], t[1]));

              Vec3f n = mesh.get_vertex_property<Vec3f>("v:normal")[v];
                  normal_coords.push_back(n);
            }

        }
        
        ///--- Vertex Buffer
        glGenBuffers(ONE, &_vpoint);
        glBindBuffer(GL_ARRAY_BUFFER, _vpoint);
        glBufferData(GL_ARRAY_BUFFER, vert_coords.size() * sizeof(vec3), &(vert_coords[0]), GL_STATIC_DRAW);
        check_error_gl();        
    
        ///--- Normal Buffer
        glGenBuffers(ONE, &_vnormal);
        glBindBuffer(GL_ARRAY_BUFFER, _vnormal);
        glBufferData(GL_ARRAY_BUFFER, normal_coords.size() * sizeof(vec3), &(normal_coords[0]), GL_STATIC_DRAW);
        check_error_gl();

        ///--- Texture UV Buffer
        glGenBuffers(ONE, &_vtex);
        glBindBuffer(GL_ARRAY_BUFFER, _vtex);
        glBufferData(GL_ARRAY_BUFFER, tex_coords.size() * sizeof(vec2), &(tex_coords[0]), GL_STATIC_DRAW);
        check_error_gl();

    
        ///--- INDEX BUFFER

        GLuint _vbo_indices;
        glGenBuffers(ONE, &_vbo_indices);
        glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, _vbo_indices);
        glBufferData(GL_ELEMENT_ARRAY_BUFFER, indices.size() * sizeof(unsigned int), &(indices[0]), GL_STATIC_DRAW);
        check_error_gl();        

        glBindVertexArray(_vao);
        check_error_gl();

        ///--- Vertex Attribute ID for Positions
        GLint vpoint_id = glGetAttribLocation(_pid, "vpoint");
        if (vpoint_id >= 0) {
            glEnableVertexAttribArray(vpoint_id);
            check_error_gl();
            glBindBuffer(GL_ARRAY_BUFFER, _vpoint);
            glVertexAttribPointer(vpoint_id, 3 /*vec3*/, GL_FLOAT, DONT_NORMALIZE, ZERO_STRIDE, ZERO_BUFFER_OFFSET);
            check_error_gl();
        }

        ///--- Vertex Attribute ID for Normals
        GLint vnormal_id = glGetAttribLocation(_pid, "vnormal");
        if (vnormal_id >= 0) {
            glEnableVertexAttribArray(vnormal_id);
            glBindBuffer(GL_ARRAY_BUFFER, _vnormal);
            glVertexAttribPointer(vnormal_id, 3 /*vec3*/, GL_FLOAT, DONT_NORMALIZE, ZERO_STRIDE, ZERO_BUFFER_OFFSET);
            check_error_gl();
        }

        ///--- Vertex Attribute ID for UV
        GLint vtex_id = glGetAttribLocation(_pid, "vtex");
        if (vtex_id >= 0) {
            glEnableVertexAttribArray(vtex_id);
            glBindBuffer(GL_ARRAY_BUFFER, _vtex);
            glVertexAttribPointer(vtex_id, 2 /*vec2*/, GL_FLOAT, DONT_NORMALIZE, ZERO_STRIDE, ZERO_BUFFER_OFFSET);
            check_error_gl();
        }

        // Load texture from disk

        glGenTextures(1, &_tex);
        glBindTexture(GL_TEXTURE_2D, _tex);
        glfwLoadTexture2D("_balloon/balloon_tex.tga", 0);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
        // Texture uniforms
        GLuint tex_id = glGetUniformLocation(_pid, "tex");
        glUniform1i(tex_id, 0);
        check_error_gl();

        ///--- Unbind
        glBindVertexArray(0);
        glUseProgram(0);

    }
           
    void cleanup(){
        glDeleteTextures(1, &_tex);
    }


    void draw(const mat4& model, const mat4& view, const mat4& projection, Light& light){
        glUseProgram(_pid);
        glBindVertexArray(_vao);

        // Bind textures
        glActiveTexture(GL_TEXTURE0);
        glBindTexture(GL_TEXTURE_2D, _tex);

        //Matrices
        mat4 mv = view*model;
        glUniformMatrix4fv(glGetUniformLocation(_pid, "mv"), ONE, DONT_TRANSPOSE, mv.data());
        glUniformMatrix4fv(glGetUniformLocation(_pid, "projection"), ONE, DONT_TRANSPOSE, projection.data());

        //Setup inverse transpose
        GLuint inv_id = glGetUniformLocation(_pid, "inv_trans_mv");
        mat3 inv_mv = (view*model).block(0,0,3,3).inverse().transpose();
        glUniformMatrix3fv(inv_id, 1, GL_FALSE, inv_mv.data());

        //Light vector
        vec4 light_vec_1 = (view*vec4(light.light_pos[0],light.light_pos[1],light.light_pos[2],0.0f));
        vec3 light_vec = vec3(light_vec_1[0],light_vec_1[1],light_vec_1[2]).normalized();
        glUniform3fv(glGetUniformLocation(_pid, "light_vec"), ONE, light_vec.data());
        //and light color
        glUniform3fv(glGetUniformLocation(_pid,"light_color"),ONE,light.getLightColor().data());

        glDrawElements(GL_TRIANGLES, /*#vertices*/ 3*mesh.n_faces(), GL_UNSIGNED_INT, ZERO_BUFFER_OFFSET);

        glBindVertexArray(0);
        glUseProgram(0);
    }
};
