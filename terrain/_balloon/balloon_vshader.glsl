#version 330 core
in vec3 vpoint;
in vec3 vnormal;
in vec2 vtex;

uniform mat4 mv;
uniform mat4 projection;

out vec3 normal;
out vec2 uv;
out vec4 vpoint_mv;

void main(){
    vpoint_mv = mv * vec4(vpoint, 1.0);
    gl_Position = projection*vpoint_mv;
    normal = vnormal;
    uv = vtex;
}
