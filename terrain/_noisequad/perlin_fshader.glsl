#version 330 core
in vec2 uv; ///<uv in [0,1]x[0,1]

uniform float width;
uniform float height;
uniform int mode;
uniform int number_cell;
uniform int octaves;
uniform float lacunarity;
uniform float h;
uniform float offset;
uniform float gain;
uniform int permutations[256];

out float color;

//Perlin noise based on Perlin java implementation

//proxy for accessing permutations elements safely
int permutation(int x){
    return permutations[x%256];
}

//Interpolation function
float f(float x) {
    return 6.0 * x*x*x*x*x - 15.0 * x*x*x*x + 10.0 * x*x*x;
}

//Returns a gradient based on the value of the hash
//They all have the same length
float get_grad3(int hash, float x, float y, float z) {
    switch(hash & 0xF){
            case 0x0: return  x + y;
            case 0x1: return -x + y;
            case 0x2: return  x - y;
            case 0x3: return -x - y;
            case 0x4: return  x + z;
            case 0x5: return -x + z;
            case 0x6: return  x - z;
            case 0x7: return -x - z;
            case 0x8: return  y + z;
            case 0x9: return -y + z;
            case 0xA: return  y - z;
            case 0xB: return -y - z;
            case 0xC: return  y + x;
            case 0xD: return -y + z;
            case 0xE: return  y - x;
            case 0xF: return -y - z;
            default: return 0.0;
        }
}

//generate 3d perlin noise value corresponding to the given position.
float noise3d(vec3 pos){
    float ratio = width / float(number_cell);//<number of pixels per unit cell (length)

     //Compute coordinates of pixel in cells units
     float x = pos.x / ratio;
     float y = pos.y / ratio;
     float z = pos.z / ratio;
     int j0 = int(x);
     int i0 = int(y);
     int k0 = int(z);
     int i1 = i0 + 1;
     int j1 = j0 + 1;
     int k1 = k0 + 1;

     //Inteprolation
     float f_x = f(x-j0);
     float f_y = f(y-i0);
     float f_z = f(z-k0);

     //Mixing gradients
     float x1 = get_grad3(permutation(permutation(j0)+i0)+k0, x-j0 , y-i0  , z-k0);
     float x2 = get_grad3(permutation(permutation(j1)+i0)+k0, x-j1, y-i0 , z-k0);
     float y1 = mix(x1,x2,f_x);
     x1 =  get_grad3(permutation(permutation(j0)+i1)+k0, x-j0  , y-i1, z-k0);
     x2 = get_grad3(permutation(permutation(j1)+i1)+k0, x-j1, y-i1, z-k0   );
     float y2 = mix(x1,x2,f_x);
     float z1 = mix(y1,y2,f_y);
     x1 = get_grad3(permutation(permutation(j0)+i0)+k1, x-j0  , y-i0  , z-k1 );
     x2 = get_grad3(permutation(permutation(j1)+i0)+k1, x-j1, y-i0  , z-k1 );
     y1 = mix(x1,x2,f_x);
     x1 = get_grad3(permutation(permutation(j0)+i1)+k1, x-j0, y-i1, z-k1 );
     x2 = get_grad3(permutation(permutation(j1)+i1)+k1, x-j1, y-i1, z-k1 );
     y2 = mix(x1,x2,f_x);
     float z2 = mix(y1,y2,f_y);

     return mix(z1,z2,f_z);
}


float multifractal(vec2 pos)
 {
     float total = 1.0;
     for (int i = 0; i < octaves; i++) {
         float add = (noise3d(vec3(pos,i)) + offset) * pow(lacunarity, -h * i);
         add *= (0.5 * total);
         pos *= lacunarity;
         total += add;
     }
     return total;
 }

float fBm(vec2 pos){
    float total = 0.0f;
     for(int i = 0; i < octaves; i++){
         total += noise3d(vec3(pos,i)) * pow(lacunarity, -h * i);
         pos *= lacunarity;
     }
     return total;
}

float ridged(vec2 pos) {
    float signal = offset - abs(noise3d(vec3(pos,0)));
    //signal*= signal;<//we limit the "strength" of the variations
    float total = signal;
    float weight = 1.0;
    float frequency = 1.0;
    for(int i=1; i<octaves; ++i) {
        pos *= lacunarity;
        weight =signal*gain;
        signal = offset - abs(noise3d(vec3(pos,i)));
        signal *= weight;
        total += signal * pow(frequency, -h);
        frequency *= lacunarity;
    }
    return total-offset*1.5;//<added offset to recenter the terrain
}

void main() {
    vec2 pos = uv * vec2(width,height);//<pos of the pixel on the screen
    switch (mode){
        case 1:
            color = fBm(pos);
          break;
        case 2:
            color = multifractal(pos);
          break;
        case 3:
            color = ridged(pos);
          break;
        case 4:
            color = fBm(pos)*ridged(pos);
          break;
        case 5:
            //Mix we are finally using
            //might take a bit of time to compile on some computers
            float noise1 = fBm(pos);
            float noise2 = ridged(pos);
            float noise3 = noise3d(vec3(pos,1.0)*3)*0.8;//<we apply some scaling here because we are reusing the same parameters
            color = mix(noise1,noise2,noise3);
          break;
        default:
            color = noise3d(vec3(pos,0.0));
          break;
    }

    //The following tests perform a "closing" on the sides of the terrain, to avoid seing under the terrain on the borders

    //(not the prettiest if conditions, but the order of the conditions allows us to escape quickly when the fragment doesn't satisfy those)
    if((gl_FragCoord.x < 10.0 || gl_FragCoord.y < 10.0) && color > 0.0){
        if(gl_FragCoord.x < 10.0 && gl_FragCoord.y < 10.0){
            color = (gl_FragCoord.x)*(gl_FragCoord.y)*color/100.0 - 0.05;
        } else if (gl_FragCoord.x < 10.0){
            color = (gl_FragCoord.x)*color / 10.0 - 0.05;
        } else {
            color = (gl_FragCoord.y)*color / 10.0 - 0.05;
        }
    }

    if((gl_FragCoord.x > width - 10.0 || gl_FragCoord.y > height - 10.0) && color > 0.0){
        if(gl_FragCoord.x > width - 10.0 && gl_FragCoord.y > height - 10.0){
            color = (width - gl_FragCoord.x)*(height - gl_FragCoord.y)*color/100.0 - 0.05;
        } else if (gl_FragCoord.x > width - 10.0){
            color = (width - gl_FragCoord.x)*color / 10.0 - 0.05;
        } else {
            color = (height - gl_FragCoord.y)*color / 10.0 - 0.05;
        }
    }


}


