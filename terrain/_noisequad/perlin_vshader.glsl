#version 330 core
in vec3 vpoint;

out vec2 uv;

void main() {
    gl_Position = vec4(vpoint, 1.0);
    uv = (gl_Position.xy + vec2(1,1))*0.5;
}
