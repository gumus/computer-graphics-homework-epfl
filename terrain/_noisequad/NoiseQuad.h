#pragma once
#include "icg_common.h"
#include <random>

//Screen quad for rendering noise
class NoiseQuad{
protected:
    GLuint _vao; ///< vertex array object
    GLuint _noise_pid; ///< GLSL shader program ID
    GLuint _vbo; ///< memory buffer
    GLuint _tex; ///< Texture ID
    int _width;
    int _height;

public:
    int number_cells = 4;
    int octaves = 5;//5
    float lacunarity = 2.9;//3.2
    float h = 1.0;
    float offset = 0.7;//0.8
    float gain = 1.2;//1.1
    int mode = 5;

    NoiseQuad(int image_width, int image_height){
        this->_width = image_width;
        this->_height = image_height;
    }

    void init(){ 
        ///--- Compile the shaders
         _noise_pid = opengp::load_shaders("_noisequad/perlin_vshader.glsl", "_noisequad/perlin_fshader.glsl");
        if(! _noise_pid) exit(EXIT_FAILURE);
        glUseProgram( _noise_pid);
        
        ///--- Vertex one vertex Array
        glGenVertexArrays(1, &_vao);
        glBindVertexArray(_vao);
     
        ///--- Vertex coordinates
        {
            const GLfloat vpoint[] = { /*V1*/ -1.0f, -1.0f, 0.0f, 
                                       /*V2*/ +1.0f, -1.0f, 0.0f, 
                                       /*V3*/ -1.0f, +1.0f, 0.0f,
                                       /*V4*/ +1.0f, +1.0f, 0.0f };        
            ///--- Buffer
            glGenBuffers(1, &_vbo);
            glBindBuffer(GL_ARRAY_BUFFER, _vbo);
            glBufferData(GL_ARRAY_BUFFER, sizeof(vpoint), vpoint, GL_STATIC_DRAW);
        
            ///--- Attribute
            GLuint vpoint_id = glGetAttribLocation( _noise_pid, "vpoint");
            glEnableVertexAttribArray(vpoint_id);
            glVertexAttribPointer(vpoint_id, 3, GL_FLOAT, DONT_NORMALIZE, ZERO_STRIDE, ZERO_BUFFER_OFFSET);
        }
        

        ///--- to avoid the current object being polluted
        glBindVertexArray(0);
        glUseProgram(0);
    }
       
    void cleanup(){
        // TODO cleanup
    }
    
    //generates a vector of 256 random numbers
    std::vector<int> generateGrid(){
        std::random_device rd;
        std::mt19937 e2(rd());
        std::uniform_real_distribution<> dist(0, 256);
        std::vector<int> hash_table;
        for(int i = 0; i < 256; i++){
            float value = dist(e2);
            hash_table.push_back(value);
        }
        return hash_table;
    }

    void draw(){
        //We generate 256 random numbers
        std::vector<int> permutations = generateGrid();
        glUseProgram(_noise_pid);
        glBindVertexArray(_vao);
        //Many parameters to pass to the shaders
            glUniform1f(glGetUniformLocation( _noise_pid, "width"), _width);
            glUniform1f(glGetUniformLocation( _noise_pid, "height"), _height);
            glUniform1i(glGetUniformLocation( _noise_pid, "mode"),mode);
            glUniform1i(glGetUniformLocation( _noise_pid, "number_cell"),number_cells);
            glUniform1i(glGetUniformLocation( _noise_pid, "octaves"), octaves);
            glUniform1f(glGetUniformLocation( _noise_pid, "lacunarity"),lacunarity);
            glUniform1f(glGetUniformLocation( _noise_pid, "h"),h);
            glUniform1f(glGetUniformLocation( _noise_pid, "offset"),offset);
            glUniform1f(glGetUniformLocation( _noise_pid, "gain"),gain);
            glUniform1iv(glGetUniformLocation(_noise_pid, "permutations"),permutations.size(),  &permutations[0]);
            glDrawArrays(GL_TRIANGLE_STRIP, 0, 4);        
        glBindVertexArray(0);        
        glUseProgram(0);
        std::cout << "Filled grid of size: " << number_cells <<  std::endl;
    }
};
