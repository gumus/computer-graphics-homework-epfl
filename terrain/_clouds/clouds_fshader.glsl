#version 330 core
in vec2 uv; ///<uv in [0,1]x[0,1]

uniform float width;
uniform float height;
uniform float time;
uniform int permutations[256];

out vec4 color;

int permutation(int x){
    return permutations[x%256];
}

float f(float x) {
    return 6.0 * x*x*x*x*x - 15.0 * x*x*x*x + 10.0 * x*x*x;
}


float get_grad3(int hash, float x, float y, float z) {
    switch(hash & 0xF){
            case 0x0: return  x + y;
            case 0x1: return -x + y;
            case 0x2: return  x - y;
            case 0x3: return -x - y;
            case 0x4: return  x + z;
            case 0x5: return -x + z;
            case 0x6: return  x - z;
            case 0x7: return -x - z;
            case 0x8: return  y + z;
            case 0x9: return -y + z;
            case 0xA: return  y - z;
            case 0xB: return -y - z;
            case 0xC: return  y + x;
            case 0xD: return -y + z;
            case 0xE: return  y - x;
            case 0xF: return -y - z;
            default: return 0.0; // never happens
        }
}

float noise3d(vec3 pos){
    float ratio = width / 4.0;//<number of pixels per unit cell (length)
     //Compute coordinates of pixel in cells units
     float x = pos.x / ratio;
     float y = pos.y / ratio;
     float z = pos.z / ratio;
     int j0 = int(x);
     int i0 = int(y);
     int k0 = int(z);
     int i1 = i0 + 1;
     int j1 = j0 + 1;
     int k1 = k0 + 1;

     float f_x = f(x-j0);
     float f_y = f(y-i0);
     float f_z = f(z-k0);

     float x1 = get_grad3(permutation(permutation(j0)+i0)+k0, x-j0 , y-i0  , z-k0);
     float x2 = get_grad3(permutation(permutation(j1)+i0)+k0, x-j1, y-i0 , z-k0);
     float y1 = mix(x1,x2,f_x);
     x1 =  get_grad3(permutation(permutation(j0)+i1)+k0, x-j0  , y-i1, z-k0);
     x2 = get_grad3(permutation(permutation(j1)+i1)+k0, x-j1, y-i1, z-k0   );
     float y2 = mix(x1,x2,f_x);
     float z1 = mix(y1,y2,f_y);
     x1 = get_grad3(permutation(permutation(j0)+i0)+k1, x-j0  , y-i0  , z-k1 );
     x2 = get_grad3(permutation(permutation(j1)+i0)+k1, x-j1, y-i0  , z-k1 );
     y1 = mix(x1,x2,f_x);
     x1 = get_grad3(permutation(permutation(j0)+i1)+k1, x-j0, y-i1, z-k1 );
     x2 = get_grad3(permutation(permutation(j1)+i1)+k1, x-j1, y-i1, z-k1 );
     y2 = mix(x1,x2,f_x);
     float z2 = mix(y1,y2,f_y);

     return mix(z1,z2,f_z);
}

float fBm(vec2 pos){
    float total = 0.0f;
     for(int i = 0; i < 4; i++){
         total += noise3d(vec3(pos,time*10)) * pow(2.0, -1.0 * i);
         pos *= 2.0;
     }
     return total;
}

//The noise modifies the alpha value
//We animate by moving along the time axis (see in fBm definition)

void main() {
   vec2 pos = uv * vec2(width,height);//<pos of the pixel on the screen
   float noise = fBm(pos)+0.5;
   color = vec4(vec3(1,1,1),noise);
}


