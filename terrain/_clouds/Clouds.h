#pragma once
#include "icg_common.h"
#include <random>

class Cloud{
protected:
    GLuint _vao; ///< vertex array object
    GLuint _noise_pid; ///< GLSL shader program ID
    GLuint _vbo; ///< memory buffer
    GLuint _tex; ///< Texture ID
    std::vector<int> permutations;
public:

    void init(){
        //Basically a plane floating over the terrain
        ///--- Compile the shaders
         _noise_pid = opengp::load_shaders("_clouds/clouds_vshader.glsl", "_clouds/clouds_fshader.glsl");
        if(! _noise_pid) exit(EXIT_FAILURE);
        glUseProgram( _noise_pid);
        
        ///--- Vertex one vertex Array
        glGenVertexArrays(1, &_vao);
        glBindVertexArray(_vao);
     
        ///--- Vertex coordinates
        {
            const GLfloat vpoint[] = { /*V1*/ -1.0f, -1.0f, 0.0f, 
                                       /*V2*/ +1.0f, -1.0f, 0.0f, 
                                       /*V3*/ -1.0f, +1.0f, 0.0f,
                                       /*V4*/ +1.0f, +1.0f, 0.0f };        
            ///--- Buffer
            glGenBuffers(1, &_vbo);
            glBindBuffer(GL_ARRAY_BUFFER, _vbo);
            glBufferData(GL_ARRAY_BUFFER, sizeof(vpoint), vpoint, GL_STATIC_DRAW);
        
            ///--- Attribute
            GLuint vpoint_id = glGetAttribLocation( _noise_pid, "vpoint");
            glEnableVertexAttribArray(vpoint_id);
            glVertexAttribPointer(vpoint_id, 3, GL_FLOAT, DONT_NORMALIZE, ZERO_STRIDE, ZERO_BUFFER_OFFSET);
        }
        

        ///--- to avoid the current object being polluted
        glBindVertexArray(0);
        glUseProgram(0);

        //Fill the permutations array with random numbers
        permutations = generateGrid();
    }
       
    void cleanup(){
        // TODO cleanup
    }
    
    std::vector<int> generateGrid(){
        std::random_device rd;
        std::mt19937 e2(rd());
        std::uniform_real_distribution<> dist(0, 256);
        std::vector<int> hash_table;
        for(int i = 0; i < 256; i++){
            float value = dist(e2);
            hash_table.push_back(value);
        }
        return hash_table;
    }

    void draw(mat4 model, mat4 view, mat4 projection, float time){

        glUseProgram(_noise_pid);
        glBindVertexArray(_vao);      
            glUniform1f(glGetUniformLocation( _noise_pid, "width"), _width);
            glUniform1f(glGetUniformLocation( _noise_pid, "height"), _height);
            glUniform1iv(glGetUniformLocation(_noise_pid, "permutations"),permutations.size(),  &permutations[0]);
            mat4 mvp = projection*view*model;
            GLuint mvp_id = glGetUniformLocation(_noise_pid, "mvp");
            glUniformMatrix4fv(mvp_id, 1, GL_FALSE, mvp.data());
            glUniform1f(glGetUniformLocation( _noise_pid, "time"),time);
            glDrawArrays(GL_TRIANGLE_STRIP, 0, 4);        
        glBindVertexArray(0);        
        glUseProgram(0);
    }
};
