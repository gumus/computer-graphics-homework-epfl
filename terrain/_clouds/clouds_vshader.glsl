#version 330 core
in vec3 vpoint;
uniform mat4 mvp;
out vec2 uv;

void main() {
    //Manual scaling and placement
    gl_Position = mvp*vec4(20*vpoint.x,vpoint.z+2.0, 20*vpoint.y, 1.0);
    uv = (vpoint.xy + vec2(1,1))*0.5;
}
