#version 330 core

layout(triangles) in;

// Three lines will be generated: 6 vertices
layout(line_strip, max_vertices=4) out;

in vec3 vnormal [];
in vec3 normal_mv [];
in vec3 light_dir [];
in vec3 view_dir [];
in float height [];

uniform mat4 mv;
uniform mat4 projection;

out vec3 normal_mv1;
out vec3 light_dir1;
out vec3 view_dir1;
out float height1;
out vec3 vnormal1;
out vec3 vertex_color;

void main()
{
  int i;
  for(int i = 0; i < gl_in.length (); ++i)
      {

          gl_Position = projection* mv * gl_in[i].gl_Position;
          vertex_color = vnormal[i];

          light_dir1 = light_dir[i];
          normal_mv1 = normal_mv[i];
          view_dir1 = view_dir[i];
          height1 = height[i];
          vnormal1 = vnormal[i];
          EmitVertex();

          gl_Position = projection*mv * (gl_in[i].gl_Position + (vec4(vnormal[i], 0) * 0.05));
          vertex_color = vnormal[i];
          EmitVertex();

          EndPrimitive();

      }
}
