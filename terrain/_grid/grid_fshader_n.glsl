#version 330 core
in float height1;
in vec3 normal_mv1;
in vec3 light_dir1;
in vec3 view_dir1;
in vec3 vertex_color;
in vec3 vnormal1;

out vec3 color;

void main() {
    color = vertex_color;
}


