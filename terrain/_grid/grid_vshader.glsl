#version 330 core
in vec2 position;

uniform sampler2D noise_tex;
uniform mat4 model;
uniform mat4 projection;
uniform mat4 mv;
uniform mat3 inv_trans_mv;
uniform mat4 depth_vp_offset;
uniform float scale_factor;
uniform bool clip;
uniform bool fast;

out vec2 uv;
out vec3 view_dir;
out float height;
out float gl_ClipDistance[1];
out vec3 distance_to_cam;
out vec4 shadow_coord;

void main() {

    //Computing texture coordinates
    uv = (position + vec2(1.0,1.0))*0.5;
    //We read the height we want to give to the vertex
    height = texture(noise_tex, uv).r * scale_factor;

    //Clipping for reflection rendering
    if (clip){
     gl_ClipDistance[0] = dot(vec4(position.x,height,-position.y,1.0),vec4(0,1,0,0));
    }

    //Computing position
    vec3 pos_3d = vec3(position.x,height,-position.y);
    vec4 vpoint_mv = mv * vec4(pos_3d, 1.0);
    gl_Position = projection * vpoint_mv;

    if(!fast){//We are not currently rendering the terrain for the shadow map
        //Computing shadow map coordinates
        shadow_coord = depth_vp_offset * model * vec4(position.x,height,-position.y,1.0);
        ///compute the view direction view_dir.
        view_dir = - vpoint_mv.xyz;
        distance_to_cam = view_dir;
        view_dir = normalize(view_dir);//<camera space
    }





}
