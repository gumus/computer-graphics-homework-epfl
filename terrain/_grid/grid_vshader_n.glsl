#version 330 core
in vec2 position;

uniform mat4 model;
uniform mat4 view;
uniform mat4 projection;
uniform mat4 mv;
uniform mat3 inv_trans_mv;
uniform sampler2D noise_tex;
uniform float scale_factor;
uniform bool clip;
uniform vec3 light_vec;

out vec3 normal_mv;
out vec3 light_dir;
out vec3 view_dir;
out vec3 vnormal;
out float height;


void main() {

    //
    //Computing texture coordinates
    vec2 uv = (position + vec2(1.0,1.0))*0.5;
    height = texture(noise_tex, uv).r * scale_factor;



    //Computing position
    vec3 pos_3d = vec3(position.x,height,-position.y);
    vec4 vpoint_mv = mv * vec4(pos_3d, 1.0);
    gl_Position = vec4(pos_3d,1.0);//<it's normal, for geometry shader

    //Finding neighbor Pixels
    vec2 dimensions = textureSize(noise_tex,0);
   /* vec3 P = vec3(uv.x * dimensions.x,height * dimensions.x,uv.y * dimensions.y);
    float smallvalue = 5;

    vec3 neig1 = vec3(int(uv.x * dimensions.x) + smallvalue, int(uv.y * dimensions.y),texture(noise_tex,vec2((int(uv.x * dimensions.x) + smallvalue) / dimensions.x, int(uv.y * dimensions.y) / dimensions.x)).r * scale_factor * dimensions.x);
    vec3 neig2 = vec3(int(uv.x * dimensions.x), int(uv.y * dimensions.y) + smallvalue,texture(noise_tex,vec2(int(uv.x * dimensions.x) / dimensions.x, (int(uv.y * dimensions.y) + smallvalue) / dimensions.x)).r * scale_factor * dimensions.x);

    //Computations in world space
    float height1 = texture(noise_tex,vec2(uv.x + smallvalue/ dimensions.x,uv.y)).r;
    vec3 neig1 = vec3(uv.x * dimensions.x + smallvalue,height1*scale_factor*dimensions.x,-uv.y * dimensions.y);

    float height2 = texture(noise_tex,vec2(uv.x ,uv.y + smallvalue / dimensions.y)).r;
    vec3 neig2 = vec3(uv.x * dimensions.x,height2*scale_factor*dimensions.y,-(uv.y * dimensions.y + smallvalue));

    vec3 tangent = neig1 - P;
    vec3 bitangent = neig2 - P;*/

    vec2 pixel_coords = uv * dimensions.xy;
    vec3 step = vec3(1.0 ,1.0 ,0.0f);
    float right = texture(noise_tex,(pixel_coords + step.xz)/dimensions.x).r * scale_factor;
    float left = texture(noise_tex,(pixel_coords - step.xz)/dimensions.x).r * scale_factor;
    float top = texture(noise_tex,(pixel_coords + step.zy)/dimensions.y).r * scale_factor;
    float bottom = texture(noise_tex,(pixel_coords - step.zy)/dimensions.y).r * scale_factor;

    vec3 tangent = vec3(2.0f / dimensions.x,right - left,0);
    vec3 bitangent = vec3(0.0,top - bottom,-2.0f / dimensions.y);
     vnormal = normalize(cross(tangent,bitangent));

    /// 1) compute the normal using the model_view matrix.
    normal_mv = inv_trans_mv * vnormal;
    normal_mv = normalize(normal_mv);//<camera space


    light_dir = normalize(light_vec);
    //Lighting directonnal = compute once in C++ code, already in camera space


    /// 3) compute the view direction view_dir.
    view_dir = - vpoint_mv.xyz;
    view_dir = normalize(view_dir);//<camera space



}
