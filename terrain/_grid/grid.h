#pragma once
#include "icg_common.h"
#include "../_light/light.h"

class Grid  {
protected:
    GLuint _vao;          ///< vertex array object
    GLuint _vbo_position; ///< memory buffer for positions
    GLuint _vbo_index;    ///< memory buffer for indice
    GLuint _pid;          ///< GLSL shader program ID
    GLuint _tex;          ///< Texture ID
    GLuint _vbo;
    GLuint _tex_noise;  ///<for texture noise
    GLuint _shadow_map;
    GLuint _tex_canyon;
    GLuint _tex_cold;
    GLuint _tex_grass;
    GLuint _tex_dessert;
    GLuint _glow;
    GLuint _num_indices;  ///< number of vertices to render
    mat4 identity;
    
public:
    float scale_factor = 0.25;
    float bias = 0.003;

    void init(GLuint noise_tex,int size_grid, GLuint shadow_map, bool normal_mode = false){
        // Compile the shaders
        if (normal_mode){
        _pid = opengp::load_shaders("_grid/grid_vshader_n.glsl", "_grid/grid_fshader_n.glsl","_grid/grid_gshader_n.glsl");
        } else {
            _pid = opengp::load_shaders("_grid/grid_vshader.glsl", "_grid/grid_fshader.glsl");
        }
        if(!_pid) exit(EXIT_FAILURE);       
        glUseProgram(_pid);
        
        // Vertex one vertex Array
        glGenVertexArrays(1, &_vao);
        glBindVertexArray(_vao);
        check_error_gl();
        // Vertex coordinates and indices
        {
            std::vector<GLfloat> vertices;
            std::vector<GLuint> indices;

            int grid_dim = size_grid;

            // The given code below are the vertices for a simple quad.
            // reaches from [-1, -1] to [1, 1].

            for(int x_pos = 0; x_pos < grid_dim; ++x_pos){
                for(int y_pos = 0; y_pos < grid_dim; ++y_pos){
                    //We add the point
                    //As we have 100 vertices on each line, we have to translate 0.5/grid_dim in order to center the mesh.
                    vertices.push_back((x_pos-grid_dim*0.5f)*(2.0f/(float)grid_dim)+(0.5f/(float)grid_dim));
                    vertices.push_back((y_pos-grid_dim*0.5f)*(2.0f/(float)grid_dim)+(0.5f/(float)grid_dim));   

                    if (y_pos != grid_dim - 1  && x_pos != grid_dim - 1){ //The last row/column has no right/top neighbour
                        //We use the GL_TRIANGLES method
                        //The index corresponding to (x_pos,y_pos) is i = (grid_dim)*y_pos+x_pos
                        indices.push_back(grid_dim*y_pos+x_pos);
                        indices.push_back(grid_dim*y_pos+x_pos+1);
                        indices.push_back(grid_dim*(y_pos+1)+x_pos+1);
                        indices.push_back(grid_dim*y_pos+x_pos);
                        indices.push_back(grid_dim*(y_pos+1)+x_pos+1);
                        indices.push_back(grid_dim*(y_pos+1)+x_pos);
                    }
                }
            }

            _num_indices = indices.size();
            // position buffer
            glGenBuffers(1, &_vbo_position);
            glBindBuffer(GL_ARRAY_BUFFER, _vbo_position);
            glBufferData(GL_ARRAY_BUFFER, vertices.size() * sizeof(GLfloat), &vertices[0], GL_STATIC_DRAW);

            // vertex indices
            glGenBuffers(1, &_vbo_index);
            glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, _vbo_index);
            glBufferData(GL_ELEMENT_ARRAY_BUFFER, indices.size() * sizeof(GLuint), &indices[0], GL_STATIC_DRAW);

            // position shader attribute
            GLuint loc_position = glGetAttribLocation(_pid, "position");
            glEnableVertexAttribArray(loc_position);
            glVertexAttribPointer(loc_position, 2, GL_FLOAT, DONT_NORMALIZE, ZERO_STRIDE, ZERO_BUFFER_OFFSET);
        }


        ///--- Load/Assign texture

        //Noise texture
        this->_tex_noise = noise_tex;
        glBindTexture(GL_TEXTURE_2D, _tex_noise);

        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);

        GLuint tex_id = glGetUniformLocation(_pid, "noise_tex");
        glUniform1i(tex_id, 0 /*GL_TEXTURE0*/);

        //CANYON Texture TEXTURE 1
        glGenTextures(1, &_tex_canyon);
        glBindTexture(GL_TEXTURE_2D, _tex_canyon);
        glfwLoadTexture2D("_grid/canyon.tga", 0);

        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);

        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);

        GLuint tex_canyon_id = glGetUniformLocation(_pid, "tex_canyon");
        glUniform1i(tex_canyon_id, 1 /*GL_TEXTURE1*/);

        //COLD Texture TEXTURE 2
        glGenTextures(1, &_tex_cold);
        glBindTexture(GL_TEXTURE_2D, _tex_cold);
        glfwLoadTexture2D("_grid/cold.tga", 0);

        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);

        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);

        GLuint tex_cold_id = glGetUniformLocation(_pid, "tex_cold");
        glUniform1i(tex_cold_id, 2 /*GL_TEXTURE2*/);

        //GRASS Texture TEXTURE 3
        glGenTextures(1, &_tex_grass);
        glBindTexture(GL_TEXTURE_2D, _tex_grass);
        glfwLoadTexture2D("_grid/grass.tga", 0);

        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);

        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);

        GLuint tex_grass_id = glGetUniformLocation(_pid, "tex_grass");
        glUniform1i(tex_grass_id, 3 /*GL_TEXTURE3*/);

        //DESSERT Texture TEXTURE 4
        glGenTextures(1, &_tex_dessert);
        glBindTexture(GL_TEXTURE_2D, _tex_dessert);
        glfwLoadTexture2D("_grid/sahara.tga", 0);

        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);

        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);

        GLuint tex_dessert_id = glGetUniformLocation(_pid, "tex_dessert");
        glUniform1i(tex_dessert_id, 4 /*GL_TEXTURE4*/);

        //SHADOW MAP TEXTURE 5
        this->_shadow_map = shadow_map;
        glBindTexture(GL_TEXTURE_2D, _shadow_map);
        check_error_gl();
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
        check_error_gl();
        GLuint sha_id = glGetUniformLocation(_pid, "shadow_map");
        glUniform1i(sha_id, 5 /*GL_TEXTURE5*/);
        check_error_gl();

        identity << 1.0f, 0.0f, 0.0f, 0.0f,
                         0.0f, 1.0f, 0.0f, 0.0f,
                         0.0f, 0.0f, 1.0f, 0.0f,
                         0.0f, 0.0f, 0.0f, 1.0f;


        glBindVertexArray(0);
        glUseProgram(0);
    }
           
    void cleanup(){
        glDeleteBuffers(1, &_vbo_position);
        glDeleteBuffers(1, &_vbo_index);
        glDeleteVertexArrays(1, &_vao);
        glDeleteTextures(1, &_tex);
        glDeleteTextures(1, &_tex_noise);
        glDeleteTextures(1, &_tex_canyon);
        glDeleteTextures(1, &_tex_cold);
        glDeleteTextures(1, &_tex_dessert);
        glDeleteTextures(1, &_tex_grass);
        glDeleteTextures(1, &_glow);
        glDeleteTextures(1, &_shadow_map);
        glDeleteProgram(_pid);

    }
    
    void draw(const mat4& model, const mat4& view, const mat4& projection, Light& light, mat4 depth_vp_offset, bool clip = false,bool depth_rendering = false){
        //depth_rendering is for the shadow map rendering (not sure if the fshader is ignored in this case)
        //To avoid artefacts at sunset (weird projected shadows from behind), we have to disable culling for this one
        if(!depth_rendering){
            glEnable(GL_CULL_FACE);
            glCullFace(GL_FRONT);
        }

        //Clipping, for rendering the reflections
        if (clip){
            glEnable(GL_CLIP_DISTANCE0);
        }

        glUseProgram(_pid);
        glBindVertexArray(_vao);

        glUniform1f(glGetUniformLocation( _pid, "scale_factor"),scale_factor);

        // Bind textures
        glActiveTexture(GL_TEXTURE0);
        glBindTexture(GL_TEXTURE_2D, _tex_noise);

        glActiveTexture(GL_TEXTURE1);
        glBindTexture(GL_TEXTURE_2D, _tex_canyon);

        glActiveTexture(GL_TEXTURE2);
        glBindTexture(GL_TEXTURE_2D, _tex_cold);

        glActiveTexture(GL_TEXTURE3);
        glBindTexture(GL_TEXTURE_2D, _tex_grass);

        glActiveTexture(GL_TEXTURE4);
        glBindTexture(GL_TEXTURE_2D, _tex_dessert);

        glActiveTexture(GL_TEXTURE5);
        glBindTexture(GL_TEXTURE_2D, _shadow_map);

        // Setup model
        GLuint model_id = glGetUniformLocation(_pid, "model");
        glUniformMatrix4fv(model_id, 1, GL_FALSE, model.data());

        // Setup view
       /* GLuint view_id = glGetUniformLocation(_pid, "view");
        glUniformMatrix4fv(view_id, 1, GL_FALSE, view.data());*/

        //Setup model view
        GLuint mv_id = glGetUniformLocation(_pid, "mv");
        mat4 mv = view * model;
        glUniformMatrix4fv(mv_id, 1, GL_FALSE, mv.data());

        //Setup projection
        mat4 proj = projection;
        GLuint P_id = glGetUniformLocation(_pid, "projection");
        glUniformMatrix4fv(P_id, 1, GL_FALSE, proj.data());

        //Setup inverse transpose
        GLuint inv_id = glGetUniformLocation(_pid, "inv_trans_mv");
        mat3 inv_mv = mv.block(0,0,3,3).inverse().transpose();
        glUniformMatrix3fv(inv_id, 1, GL_FALSE, inv_mv.data());

        //Setup light dir in view space
        vec4 light_vec_1 = (view*vec4(light.light_pos[0],light.light_pos[1],light.light_pos[2],0.0f));
        vec3 light_vec = vec3(light_vec_1[0],light_vec_1[1],light_vec_1[2]);
        glUniform3fv(glGetUniformLocation(_pid, "light_vec"), ONE, light_vec.data());

        glUniform1f(glGetUniformLocation(_pid,"sun_altitude"),light.getSunAltitude());
        glUniform3fv(glGetUniformLocation(_pid, "light_color"), ONE, light.getLightColor().data());
        glUniform3fv(glGetUniformLocation(_pid, "deep_color"), ONE, light.getDeepColor().data());

        //Shadow map uniforms
        bool test = (depth_vp_offset != identity);
        glUniform1i(glGetUniformLocation(_pid,"shadow_enabled"),test);
        glUniformMatrix4fv(glGetUniformLocation(_pid, "depth_vp_offset"), 1, GL_FALSE, depth_vp_offset.data());
        glUniform1f(glGetUniformLocation(_pid,"bias"),bias);
        glUniform1i(glGetUniformLocation(_pid,"fast"),depth_rendering);

        //Clipping
        glUniform1i(glGetUniformLocation(_pid, "clip"),clip);

        glDrawElements(GL_TRIANGLES, _num_indices, GL_UNSIGNED_INT, 0);
        glBindVertexArray(0);        
        glUseProgram(0);

        if (clip){
            glDisable(GL_CLIP_DISTANCE0);
        }

        if(!depth_rendering){
            glDisable(GL_CULL_FACE);
        }
    }
};
