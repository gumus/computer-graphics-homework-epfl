#version 330 core
in vec2 uv;
in vec3 view_dir;
in vec3 distance_to_cam;
in float height;
in vec4 shadow_coord;

uniform sampler2D noise_tex;
uniform float scale_factor;
uniform mat3 inv_trans_mv;
uniform float sun_altitude;
uniform vec3 deep_color;
uniform vec3 light_color;
uniform sampler2D tex_canyon;
uniform sampler2D tex_cold;
uniform sampler2D tex_grass;
uniform sampler2D tex_dessert;
uniform sampler2D shadow_map;
uniform vec3 light_vec;
uniform float bias;
uniform bool fast;
uniform bool shadow_enabled;
// Poisson disk sample locations.
const vec2 poisson_disk[16] = vec2[](
   vec2(-0.94201624, -0.39906216),
   vec2(0.94558609, -0.76890725),
   vec2(-0.094184101, -0.92938870),
   vec2(0.34495938, 0.29387760),
   vec2(-0.91588581, 0.45771432),
   vec2(-0.81544232, -0.87912464),
   vec2(-0.38277543, 0.27676845),
   vec2(0.97484398, 0.75648379),
   vec2(0.44323325, -0.97511554),
   vec2(0.53742981, -0.47373420),
   vec2(-0.26496911, -0.41893023),
   vec2(0.79197514, 0.19090188),
   vec2(-0.24188840, 0.99706507),
   vec2(-0.81409955, 0.91437590),
   vec2(0.19984126, 0.78641367),
   vec2(0.14383161, -0.14100790)
);

out vec3 color;

void main() {
    color = vec3(1.0);
    if(!fast){

    //Base Color Settings
    float height_max = 1.0;

    float cold_beginning = 0.11; // Define for snow height 0 to 1
    cold_beginning = cold_beginning * height_max;

    float dessert_end = 0.02; // Define for end of dessert height 0 to 1
    dessert_end = dessert_end * height_max;

    float speed_transections = 15; // Define speed of transection(mixture of colors)
    //Settings End

    //Computing the normal at the current fragment
    vec2 dimensions = textureSize(noise_tex,0);
    vec2 pixel_coords = uv * dimensions.xy;

    vec2 step = vec2(1.0,0.0f)*2.0f;
    float right = texture(noise_tex,(pixel_coords + step.xy)/dimensions.x).r * scale_factor ;
    float left = texture(noise_tex,(pixel_coords - step.xy)/dimensions.x).r * scale_factor;
    float top = texture(noise_tex,(pixel_coords + step.yx)/dimensions.y).r * scale_factor;
    float bottom = texture(noise_tex,(pixel_coords - step.yx)/dimensions.y).r * scale_factor;

    vec3 tangent = vec3(2.0f / dimensions.x,right - left,0);
    vec3 bitangent = vec3(0.0,top - bottom,-2.0f / dimensions.y);
    vec3 vnormal = normalize(cross(tangent,bitangent));
    vec3 normal_mv = inv_trans_mv * vnormal; normal_mv = normalize(normal_mv);

    ///(Color part 1)
    ///Compute the output color by doing a look-up in the texture using the texture sampler tex.
    //color = texture(tex1D, pow(dot(normal_mv,light_dir),p)).rgb;

    //COLOR WORKS

    float height_normalized = height / height_max; height_normalized = (height_normalized > 1) ? 1 : height_normalized; height_normalized = (height_normalized < -1) ? -1 : height_normalized;

    //Terrain colors
    vec3 base_color = vec3(0.0);

    vec3 grass_color = texture(tex_grass, 40/*8.0/length(distance_to_cam) */* uv).rgb;
    vec3 cold_color = (height_normalized < dessert_end / height_max) ? texture(tex_dessert, 40 * uv).rgb : texture(tex_cold, 70 * uv).rgb;
    vec3 canyon_color = texture(tex_canyon, 30 * uv).rgb;

    //According to slope: color
    float slope_coefficient = 0;
    float slope = 1 - abs(vnormal.y);
    slope_coefficient =  slope < 0.4 ? 0 : pow((slope  - 0.4) *2,0.9);

    //Maximum Control Of slope_coefficient
    slope_coefficient  = slope_coefficient > 1 ? 1 : slope_coefficient;

    //According to height: color

    //SNOW
    float height_coefficient = 0;
    height_coefficient =  (height_normalized + (texture(noise_tex,uv * 20).r / 8) < cold_beginning ? 0 : 1);

    //DESERT
    height_coefficient =  height_normalized > dessert_end ? height_coefficient :  (dessert_end - height_normalized)  / (slope_coefficient < 0.1 ? 0.1 : slope_coefficient  ) * speed_transections;

    //Maximum Control Of height_coefficient
    height_coefficient  = height_coefficient > 1 ? 1 : height_coefficient;

    //Mixture Of textures
    base_color = slope_coefficient * (height_coefficient * cold_color + (1 - height_coefficient) * canyon_color);
    base_color +=  (1 - slope_coefficient) * (height_coefficient * cold_color + (1 - height_coefficient) * grass_color);



    //LIGHTING
    //Ambient Light
    vec3 ambient =  base_color * light_color ;
    //Diffuse Light
    float intensity = max(0.0,dot(normalize(normal_mv), normalize(light_vec)));
    vec3 diffuse = base_color * light_color;

    //No specular light for the terrain

    //I saw this trick online on lighthouse3d.com : instead of doing ambient + diffuse (which might be too bright), you do max(ambient, diffuse) basically
    color = diffuse*max(0.35,intensity);

    //Shadow map
    float shadow = 1.0;
    if(shadow_enabled){
        // shading factor from the shadow (1.0 = no shadow, 0.0 = all dark)
        float col = texture(shadow_map,shadow_coord.xy).r;
        float spread = 150.0;
        //Loop for the attenuation using poisson disk
        for (int i=0;i<16;i++){
            if ( texture( shadow_map, shadow_coord.xy + poisson_disk[i]/spread).r  <  shadow_coord.z - bias ){
                shadow -= (1.0/30.0);
            }
        }

        //Continuity with obscurity at night
        if(sun_altitude<0.1) {
            shadow = shadow*clamp((1.0-abs(sun_altitude-0.1)),0.0,1.0);
        }
    }


    //"Fog" underwater
    if (height < 0){
      color = mix(color,deep_color,clamp((-height*5+0.65),0,1));
    }
    color = shadow * color;


    }
}


