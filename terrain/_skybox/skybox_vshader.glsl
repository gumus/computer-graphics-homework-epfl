#version 330 core
in vec3 position;

uniform mat4 mvp;

out vec3 tex_position;

void main(){
    gl_Position = mvp * vec4(position, 1.0);
    tex_position  = position;
}
