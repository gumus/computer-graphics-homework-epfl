#version 330 core
in vec3 tex_position;

uniform samplerCube tex;//<cube map

out vec3 color;

void main(){
    //A cubemap need 3D texture position
    color = texture(tex,tex_position).rgb;

}
