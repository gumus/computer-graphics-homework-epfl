#pragma once
#include "icg_common.h"

class Skybox {
private:
    GLuint _vao;          ///< vertex array object
    GLuint _vbo_position; ///< memory buffer for positions
    GLuint _vbo_index;    ///< memory buffer for indices
    GLuint _pid;          ///< GLSL shader program ID
    GLuint _num_indices;  ///< number of vertices to render
    GLuint _cube_map_id;
public:
    GLuint program_id() const{
        return _pid;
    }

    void init(){
        // Compile the shaders.
        _pid = opengp::load_shaders("_skybox/skybox_vshader.glsl", "_skybox/skybox_fshader.glsl");
        if(!_pid)
          exit(EXIT_FAILURE);
        glUseProgram(_pid);

        // Vertex one vertex array
        glGenVertexArrays(1, &_vao);
        glBindVertexArray(_vao);

        // Position buffer
        const GLfloat position[] = {-1.0f, -1.0f,  1.0f,  // left, bottom, front
                                     1.0f, -1.0f,  1.0f,  // right, bottom, front
                                     1.0f,  1.0f,  1.0f,  // right, top, front
                                    -1.0f,  1.0f,  1.0f,  // left, top, front
                                    -1.0f, -1.0f, -1.0f,  // left, bottom, back
                                     1.0f, -1.0f, -1.0f,  // right, bottom, back
                                     1.0f,  1.0f, -1.0f,  // right, top, back
                                    -1.0f,  1.0f, -1.0f}; // left, top, back*/

        glGenBuffers(1, &_vbo_position);
        glBindBuffer(GL_ARRAY_BUFFER, _vbo_position);
        glBufferData(GL_ARRAY_BUFFER, sizeof(position), position, GL_STATIC_DRAW);

        // position shader attribute
        GLuint loc_position = glGetAttribLocation(_pid, "position"); ///< Fetch attribute ID for vertex positions
        glEnableVertexAttribArray(loc_position); /// Enable it
        glVertexAttribPointer(loc_position, 3, GL_FLOAT, DONT_NORMALIZE, ZERO_STRIDE, ZERO_BUFFER_OFFSET);

        // Index buffer
        const GLuint index[] = {0, 1, 2,  // Front face triangle 1
                                0, 2, 3,  // Front face triangle 2
                                1, 5, 6,  // Right face
                                1, 6, 2,
                                5, 4, 7,  // Back face
                                5, 7, 6,
                                4, 0, 3,  // Left face
                                4, 3, 7,
                                3, 2, 6,  // Top face
                                3, 6, 7,
                                1, 0, 4,  // Bottom face
                                1, 4, 5};

        _num_indices = sizeof(index) / sizeof(GLuint);

        glGenBuffers(1, &_vbo_index);
        glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, _vbo_index);
        glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(index), index, GL_STATIC_DRAW);
    
        //Loading the textures into a cubemap texture
        glGenTextures(1, &_cube_map_id);
            glBindTexture(GL_TEXTURE_CUBE_MAP, _cube_map_id);

            vector<const GLchar*> faces;
            faces.push_back("_skybox/miramar/miramar_rt.tga");
            faces.push_back("_skybox/miramar/miramar_lf.tga");
            faces.push_back("_skybox/miramar/miramar_up.tga");
            faces.push_back("_skybox/miramar/miramar_dn.tga");
            faces.push_back("_skybox/miramar/miramar_bk.tga");
            faces.push_back("_skybox/miramar/miramar_ft.tga");

            //We read each face image, and bind it to the right GL_TEXTURE_CUBE_MAP face
            for (unsigned int i = 0 ; i < 6 ; i++) {
                GLFWimage img = GLFWimage();
                glfwReadImage(faces[i],&img,0);
                glTexImage2D(GL_TEXTURE_CUBE_MAP_POSITIVE_X + i, 0,GL_RGB, 512, 512, 0, GL_RGBA, GL_UNSIGNED_BYTE, img.Data);
            }

            glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
            glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
            glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
            glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
            glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_R, GL_CLAMP_TO_EDGE);

            GLuint tex_id = glGetUniformLocation(_pid, "tex");
            glUniform1i(tex_id, 0 /*TEXTURE_0*/);

        // to avoid the current object being polluted
        glBindVertexArray(0);
    }
       
    void cleanup(){     
        glDeleteBuffers(1, &_vbo_position);
        glDeleteBuffers(1, &_vbo_index);
        glDeleteVertexArrays(1, &_vao);
    }

    void draw(const mat4& model, const mat4& view, const mat4& projection, Light& light, bool keep_translation = false){

         mat4 view_without_translation = mat4::Zero();
         //We want the skybox to follow the camera, so we remove the translation aprt of the view matrix.
        if (!keep_translation){
            view_without_translation.block(0,0,3,3) = view.block(0,0,3,3);
            view_without_translation(3,3) = 1.0;
        }else {
            view_without_translation = view;
        }

        glUseProgram(_pid);
        glBindVertexArray(_vao);
        glActiveTexture(GL_TEXTURE0);
        glBindTexture(GL_TEXTURE_CUBE_MAP, _cube_map_id);

        // Setup MVP
        mat4 MVP = projection*view_without_translation*model;
        GLuint MVP_id = glGetUniformLocation(_pid, "mvp");
        glUniformMatrix4fv(MVP_id, 1, GL_FALSE, MVP.data());

        //Draw
        glDrawElements(GL_TRIANGLES, _num_indices, GL_UNSIGNED_INT, 0);
        glBindVertexArray(0);
        glUseProgram(0);
    }
};
