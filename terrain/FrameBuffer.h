#pragma once
#include "icg_common.h"

#ifdef WITH_OPENCV
    #include "opencv2/core/core.hpp"       ///< cv::Mat
    #include "opencv2/highgui/highgui.hpp" ///< cv::imShow
    #include "opencv2/contrib/contrib.hpp" ///< cvtConvert
    #include "opencv2/imgproc/types_c.h"   ///< CV_BGRA2RGBA
#endif

class FrameBuffer{
protected:
    bool _init;
    int _width;
    int _height;


    GLuint _fbo;
    GLuint _depth_rb;
    GLuint _color_tex;
    
public:
    bool depth_only = false;//<for the shadow map rendering

    FrameBuffer(int image_width, int image_height){
        this->_width = image_width;
        this->_height = image_height;        
    }
        

    void bind(float width1 = 0.0, float height1 = 0.0) {
        if(width1 !=0.0 && height1 != 0.0 && width1 != _width && height1 != _height){
            _width = width1;
            _height = height1;
        }
        glViewport(0, 0, _width, _height);
        glBindFramebuffer(GL_FRAMEBUFFER, _fbo);
        if(!depth_only){
            const GLenum buffers[] = { GL_COLOR_ATTACHMENT0 };
            glDrawBuffers(1 /*length of buffers[]*/, buffers);
        }
    }
    
    void unbind() {
        glBindFramebuffer(GL_FRAMEBUFFER, 0);
    }

    int init(bool use_interpolation = false, bool noisemode = false) {
        ///--- Create color attachment
        {

            glGenTextures(1, &_color_tex);
            glBindTexture(GL_TEXTURE_2D, _color_tex);

            glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
            glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
    
            if(use_interpolation){
                glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
                glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
            } else {
                glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
                glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
            }
            
            ///--- Create texture for the color attachment
            if (noisemode){
                //Noise generation : we need a texture containing floats
                glTexImage2D(GL_TEXTURE_2D, 0, GL_R32F,
                         _width, _height, 0, 
                         GL_RED, GL_FLOAT, NULL); ///< how to load from buffer
            } else if(depth_only){
                //Shadowmap rendering : only need to get depth
                glTexImage2D(GL_TEXTURE_2D, 0, GL_DEPTH_COMPONENT24, _width, _height, 0, GL_DEPTH_COMPONENT, GL_FLOAT, NULL);
            }else{
                //Else normal rendering
                glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB8,
                                         _width, _height, 0,
                                         GL_RGB, GL_UNSIGNED_BYTE, NULL);
            } 
        }
        
        ///--- Create render buffer (for depth channel)
        /// If depth_only, we don't need to do it, we render the depth directly
        if(!depth_only){
            glGenRenderbuffers(1, &_depth_rb);
            glBindRenderbuffer(GL_RENDERBUFFER, _depth_rb);
            glRenderbufferStorage(GL_RENDERBUFFER, GL_DEPTH_COMPONENT32, _width, _height);
            glBindRenderbuffer(GL_RENDERBUFFER, 0);
        }
    
        ///--- Tie it all together
        {
            glGenFramebuffers(1, &_fbo);
            glBindFramebuffer(GL_FRAMEBUFFER, _fbo);
            if(depth_only){
                glFramebufferTexture(GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, _color_tex, 0);
            } else {
                glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0 /*location = 0*/, GL_TEXTURE_2D, _color_tex, 0 /*level*/);
                glFramebufferRenderbuffer(GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, GL_RENDERBUFFER, _depth_rb);
            }
            if (glCheckFramebufferStatus(GL_FRAMEBUFFER) != GL_FRAMEBUFFER_COMPLETE)
                std::cerr << "!!!ERROR: Framebuffer not OK :(" << std::endl;

            if(depth_only){
                //We only want to draw depth, nothing else
                glDrawBuffer(GL_NONE);
            }
        }
            glBindFramebuffer(GL_FRAMEBUFFER, 0); ///< avoid pollution

        
        return _color_tex;
    }

    void cleanup() {
        glDeleteTextures(1, &_color_tex);
        if(!depth_only)
            glDeleteRenderbuffers(1, &_depth_rb);
        glBindFramebuffer(GL_FRAMEBUFFER, 0 /*UNBIND*/);
        glDeleteFramebuffers(1, &_fbo);
    }

    void clear(){
            glViewport(0, 0, _width, _height);
                glBindFramebuffer(GL_FRAMEBUFFER, _fbo);
                    glDrawBuffer(GL_COLOR_ATTACHMENT0);
                    glClearColor(0.0, 0.0, 0.0, 1.0);
                    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
                glBindFramebuffer(GL_FRAMEBUFFER, 0);
        }
    
public:
    void display_color_attachment(const char* title) {
#ifdef WITH_OPENCV 
        ///--- Fetch from opengl
        if(depth_only){
            //glActiveTexture(GL_TEXTURE1);
            cv::Mat image = cv::Mat(_height, _width, CV_32FC1, cv::Scalar(0));
            glBindTexture(GL_TEXTURE_2D, _color_tex);
            glGetTexImage(GL_TEXTURE_2D, 0, GL_DEPTH_COMPONENT, GL_FLOAT, image.data);
            glBindTexture(GL_TEXTURE_2D, 0);
            ///--- Postprocess before showing/saving
            cv::flip(image, image, 0 /*flip rows*/ ); ///< OpenGL / OpenCV top left origin

            cv::imshow(title, image);
        } else {
            cv::Mat image = cv::Mat(_height, _width, CV_8UC3, cv::Scalar(0));
            glBindTexture(GL_TEXTURE_2D, _color_tex);
            glGetTexImage(GL_TEXTURE_2D, 0, GL_RGB, GL_UNSIGNED_BYTE, image.data);
            glBindTexture(GL_TEXTURE_2D, 0);
            ///--- Postprocess before showing/saving
            cv::flip(image, image, 0 /*flip rows*/ ); ///< OpenGL / OpenCV top left origin
            cv::cvtColor(image, image, CV_BGRA2RGBA); ///< OpenCV uses BGRA
            cv::imshow(title, image);
        }
        // cv::waitKey(0); ///< wait for key to be pressed (not necessary with opengl loop)
#else
        std::cout <<"!!!WARNING: sorry, you do not seem to have OpenCV properly configured." << std::endl;
#endif
    }
};
