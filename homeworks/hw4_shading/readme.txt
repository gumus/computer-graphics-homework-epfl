---- Homework 4 ----
----- Group 10 -----
--------------------
Orçun Gümüş
Kıvanç Kamay
Simon Rodriguez

We put the executable along with the shaders in the "result" folder.

1.) We can see how the Phong shader produce smoother lighting, especially for specular lighting, where the elements are much smoother than with Gouraud Shading.
2.) Here we pick grey shades in the 1D texture. We get using using the R component of the color (only non-zero component)
3.) We bind the 2D color texture and pick colors in it using (N.L) and (R.V)^p
4.) We are using dFdx and dFdy to compute gradients along the x and y axis, and then build a normal with the cross product.
	This normal is used for all fragments corresponding to a given triangle.
5.)

Note : to calculate the light vector, with the formula described in the course and the book (using the light position and point position) we weren't getting the expected results, and thus had to adapt the calculation by tweaking.