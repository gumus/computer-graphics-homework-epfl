// Copyright (C) 2014 - LGG EPFL
#version 330 core
uniform mat4 projection;
uniform mat4 model;
uniform mat4 view;
uniform vec3 light_pos;

in vec3 vpoint;
in vec3 vnormal;


out vec3 normal_mv;
out vec3 light_dir;
out vec3 view_dir;


void main() {
    mat4 MV = view * model;
    vec4 vpoint_mv = MV * vec4(vpoint, 1.0);
    gl_Position = projection * vpoint_mv;

    /// 1) compute the normal using the model_view matrix.
    normal_mv = (inverse(transpose(model)) * vec4(vnormal,0.0)).xyz;
    normal_mv = normalize(normal_mv);

    /// 2) compute the light direction light_dir.
    light_dir = vec3(light_pos.x, -light_pos.z, -light_pos.y);
    light_dir = normalize(light_dir);

    /// 3) compute the view direction view_dir.
    view_dir = - vpoint_mv.xyz;
    view_dir = normalize(view_dir);


    ///>>>>>>>>>> TODO >>>>>>>>>>>
    /// TODO 1.1: Phong shading.
    /// 1) compute normal_mv using the model_view matrix.
    /// 2) compute the light direction light_dir.
    /// 3) compute the view direction view_dir.
    ///<<<<<<<<<< TODO <<<<<<<<<<<
}
