// Copyright (C) 2014 - LGG EPFL
#version 330 core

uniform vec3 Ia, Id, Is;
uniform vec3 ka, kd, ks;
uniform float p;

in vec4 vpoint_mv;
in vec3 light_dir, view_dir;

out vec3 color;

void main() {
    color = vec3(0.0,0.0,0.0);

    ///>>>>>>>>>> TODO >>>>>>>>>>>
    /// TODO 4.2: Flat shading.
    /// 1) compute triangle normal using dFdx and dFdy
    /// 1) compute ambient term.
    /// 2) compute diffuse term.
    /// 3) compute specular term.
    ///<<<<<<<<<< TODO <<<<<<<<<<<


    vec3 X = dFdx(vpoint_mv).xyz;
    vec3 Y = dFdy(vpoint_mv).xyz;
    vec3 normal_mv = normalize(cross(X,Y));

    /// 4) compute the R direction
    vec3 reflect_dir = 2.0f * dot(light_dir,normal_mv) * normal_mv - light_dir;
    reflect_dir = normalize(reflect_dir);
    float phongTerm = pow(max(0,dot(reflect_dir ,view_dir)),p);

    color = Ia * ka + Id * kd * max(0,dot(normal_mv, light_dir)) + Is * ks * phongTerm ;

}
