// Copyright (C) 2014 - LGG EPFL
#version 330 core

uniform vec3 Ia, Id, Is;
uniform vec3 ka, kd, ks;


uniform float p;

in vec3 normal_mv;
in vec3 light_dir;
in vec3 view_dir;

out vec3 color;


void main() {
    color = vec3(0.0,0.0,0.0);



    //Calculating r vector
    vec3 r_vec = 2.0f * dot(light_dir,normal_mv) * normal_mv - light_dir;
    r_vec = normalize(r_vec);
    ///Compute the output color by doing a look-up in the texture using the texture sampler tex.
    //color = texture(tex1D, pow(dot(normal_mv,light_dir),p)).rgb;
    vec3 ambient = ka * Ia;
    vec3 diffuse = kd * Id * max(0,dot(normal_mv, light_dir));
    vec3 specular = ks * Is * pow(max(0,dot(r_vec ,view_dir)),p);

    color = ambient + diffuse + specular;

    ///>>>>>>>>>> TODO >>>>>>>>>>>
    /// TODO 1.2: Phong shading.
    /// 1) compute ambient term.
    /// 2) compute diffuse term.
    /// 3) compute specular term.
    ///<<<<<<<<<< TODO <<<<<<<<<<<
}
