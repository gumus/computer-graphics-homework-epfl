// Copyright (C) 2014 - LGG EPFL
#version 330 core 
uniform vec3 Ia, Id, Is;
uniform vec3 ka, kd, ks;
uniform float p;

uniform mat4 projection;
uniform mat4 model;
uniform mat4 view;
uniform vec3 light_pos;

in vec3 vpoint;
in vec3 vnormal;


out vec3 vertex_color;

void main() {

    vec3 camera_pos = vec3(2.0f, 2.0f, 2.0f);
    mat4 MV = view * model;
    vec4 vpoint_mv = MV * vec4(vpoint, 1.0);
    gl_Position = projection * vpoint_mv;


    vec3 light_dir;
    vec3 normal_mv;
    vec3 view_dir;

    ///Computations are done in camera space

    /// 1) compute the normal using the model_view matrix.
    normal_mv = (inverse(transpose(model)) * vec4(vnormal,0.0)).xyz;
    normal_mv = normalize(normal_mv);

    /// 2) compute the light direction light_dir.
    //Normally we should have something like light_pos - vpoint_mv (cf book), but this is not working
    light_dir = vec3(light_pos.x, -light_pos.z, -light_pos.y) ;
    light_dir = normalize(light_dir);

    /// 3) compute the view direction view_dir.
    view_dir = - vpoint_mv.xyz;
    view_dir = normalize(view_dir);

    /// 4) compute the R direction
    vec3 reflect_dir = 2.0f * dot(light_dir,normal_mv) * normal_mv - light_dir;
    reflect_dir = normalize(reflect_dir);
    float phongTerm = pow(dot(reflect_dir,view_dir),p);

    vertex_color = Ia * ka + Id * kd * max(0,dot (normal_mv, light_dir)) + Is * ks * max(0,phongTerm) ;

    /*
    vec4 light_dir;
    vec4 normal_mv;
    vec4 view_dir;
    normal_mv = vec4(vnormal, 1.0) * transpose(MV);
    light_dir = normalize(vpoint_mv - vec4(light_pos,1));
    view_dir =  normalize(vpoint_mv - vec4(camera_pos,1));
    vec4 reflect_dir = reflect(-light_dir, normal_mv);

    vec4 surfaceNormal = normalize(normal_mv);

    float cosAngIncidence = dot(vec4(vnormal,1), light_dir);
    cosAngIncidence = clamp(cosAngIncidence, 0, 1);


    float phongTerm = dot(view_dir, reflect_dir);
    phongTerm = clamp(phongTerm, 0, 1);
    phongTerm = cosAngIncidence != 0.0 ? phongTerm : 0.0;
    phongTerm = pow(phongTerm, p);
    */



}
