// Copyright (C) 2014 - LGG EPFL
#version 330 core

uniform float p;
uniform sampler2D tex2D;


in vec3 normal_mv;
in vec3 light_dir;
in vec3 view_dir;

out vec3 color;

void main() {
    color = vec3(0.0,0.0,0.0);

    //Calculating r vector
    vec3 r_vec = 2.0f * dot(light_dir,normal_mv) * normal_mv - light_dir;
    r_vec = normalize(r_vec);
    ///Compute the output color by doing a look-up in the texture using the texture sampler tex.
    color = texture(tex2D,vec2(dot(normal_mv,light_dir),pow(dot(r_vec,view_dir),p))).rgb;

}
