// Copyright (C) 2014 - LGG EPFL
#version 330 core

out vec3 color;

in vec3 vertex_color;

void main() {
    color = vec3(0.0,0.0,0.0);


    /*const vec3 COLORS[6] = vec3[](
        vec3(1.0,0.0,0.0),
        vec3(0.0,1.0,0.0),
        vec3(0.0,0.0,1.0),
        vec3(1.0,1.0,0.0),
        vec3(0.0,1.0,1.0),
        vec3(1.0,0.0,1.0));*/


    color = vertex_color;


    ///>>>>>>>>>> TODO >>>>>>>>>>>
    /// TODO 0.1: Gouraud shading.
    ///<<<<<<<<<< TODO <<<<<<<<<<<
}
