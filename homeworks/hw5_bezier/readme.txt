---- Homework 5 ----
----- Group 10 -----
--------------------
Orçun Gümüş
Kıvanç Kamay
Simon Rodriguez

We put the executable along with the shaders in the "result" folder.

1.) In this part, we have to take into account trackball_matrix to reflect the movements of the camera in trackball mode. Apart from "unproject", other methods are the same as in the lab session.
2.) We directly pass the cam_points vectors to the Bezier object, which then retrieve the position from each Point when generating the hulls. This allows us to pass an unrestricted number of control points.
3.) We now create two bezier curves (we colored the control points of the cam_look curve in blue to be able do distinguish them). Once both curves are set, pressing '2' switches to the camera_mode. The animation duration is set to 10 sec.

