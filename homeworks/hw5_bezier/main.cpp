#include "icg_common.h"
#include "trackball.h"

#include "_quad/Quad.h"
#include "_cube/cube.h"
#include "_point/point.h"
#include "_bezier/bezier.h"

enum NAVIGATION_MODE {
    TRACKBALL,
    BEZIER
} navmode;

int window_width = 800;
int window_height = 600;

mat4 projection;
mat4 view;
mat4 model;

GLuint _pid_bezier;
GLuint _pid_point;
GLuint _pid_point_selection;

///----------------- Trackball -----------------
mat4 trackball_matrix;
mat4 old_trackball_matrix;
Trackball trackball;

vec2 transform_screen_coords(int x, int y) {
    return vec2(2.0f * (float)x / window_width - 1.0f,
                1.0f - 2.0f * (float)y / window_height);
}

void trackball_button(int button, int action) {
    if (button == GLFW_MOUSE_BUTTON_LEFT && action == GLFW_PRESS) {
        int x_i, y_i;
        glfwGetMousePos(&x_i, &y_i);
        vec2 p = transform_screen_coords(x_i, y_i);
        trackball.begin_drag(p.x(), p.y());
        old_trackball_matrix = trackball_matrix;  // Store the current state of the model matrix.
    }
}

void trackball_pos(int x, int y) {
    if (glfwGetMouseButton(GLFW_MOUSE_BUTTON_LEFT) == GLFW_PRESS) {
        vec2 p = transform_screen_coords(x, y);
        trackball_matrix = trackball.drag(p.x(), p.y()) * old_trackball_matrix;
    }
}
///---------------------------------------------------
Quad quad;

BezierCurve cam_pos_curve;
BezierCurve cam_look_curve;
Cube cube;


std::vector<ControlPoint> cam_pos_points;
std::vector<ControlPoint> cam_look_points;
int selected_point;

double start_time; ///< needed for the animation

void init(){
    /// Compile the shaders here to avoid the duplication
    _pid_bezier = opengp::load_shaders("bezier_vshader.glsl", "bezier_fshader.glsl");
    if(!_pid_bezier) exit(EXIT_FAILURE);

    _pid_point = opengp::load_shaders("point_vshader.glsl", "point_fshader.glsl");
    if(!_pid_point) exit(EXIT_FAILURE);

    _pid_point_selection = opengp::load_shaders("point_selection_vshader.glsl", "point_selection_fshader.glsl");
    if(!_pid_point_selection) exit(EXIT_FAILURE);

    glClearColor(1,1,1, /*solid*/1.0 );    
    glEnable(GL_DEPTH_TEST);
    quad.init();
    cube.init();

    ///--- init cam_pos_curve
    cam_pos_curve.init(_pid_bezier);

    cam_pos_points.push_back(ControlPoint(-0.98, 1.414, 0.7, 0));
    cam_pos_points.push_back(ControlPoint(-1.386, -1.498, 0.7, 1));
    cam_pos_points.push_back(ControlPoint(2.03, -1.414, 0.56, 2));
    cam_pos_points.push_back(ControlPoint(1.064, 0.938, 0.42, 3));
    cam_pos_points.push_back(ControlPoint(0.406, 1.372, 0.28, 4));
    cam_pos_points.push_back(ControlPoint(-1.008, 1.442, 0.21, 5));
    cam_pos_points.push_back(ControlPoint(-0.616, -3.156, 0.14, 6));

    for (unsigned int i = 0; i < cam_pos_points.size(); i++) {
        cam_pos_points[i].id() = i;
        cam_pos_points[i].init(_pid_point, _pid_point_selection);
    }
    cam_pos_curve.set_points(cam_pos_points);

    ///===================== DONE =====================
    ///--- TODO H5.3: Set points for cam_look_curve here
    /// Don't forget to set correct point ids.
    /// ===============================================

    ///--- init cam_look_curve
    cam_look_curve.init(_pid_bezier);

    cam_look_points.push_back(ControlPoint(-0.19, -0.02, 0.15, 0));
    cam_look_points.push_back(ControlPoint(-0.18, -0.32, 0.15, 1));
    cam_look_points.push_back(ControlPoint(0.27, -0.24, 0.17, 2));
    cam_look_points.push_back(ControlPoint(-0.01, 0.22, 0.3, 3));
    cam_look_points.push_back(ControlPoint(-0.252, 0.507, 0.39, 4));
    cam_look_points.push_back(ControlPoint(-0.496, -0.042, 0.47, 5));
    cam_look_points.push_back(ControlPoint(-0.03, -0.34, 0.48, 6));

    for (unsigned int i = 0; i < cam_look_points.size(); i++) {
        //We shift indices by cam_pos_points.size(), to be sure no point on cam_look will have the same id as a point of cam_pos
        cam_look_points[i].id() = i + cam_pos_points.size();
        cam_look_points[i].init(_pid_point, _pid_point_selection);
    }
    cam_look_curve.set_points(cam_look_points);

    ///--- Setup view-projection matrix
    float ratio = window_width / (float) window_height;
    projection = Eigen::perspective(45.0f, ratio, 0.1f, 10.0f);

    vec3 cam_pos(2.0f, 2.0f, 2.0f);
    vec3 cam_look(0.0f, 0.0f, 0.0f);
    vec3 cam_up(0.0f, 0.0f, 1.0f);

    view = Eigen::lookAt(cam_pos, cam_look, cam_up);
    model = mat4::Identity();
    trackball_matrix = mat4::Identity();
    selected_point = -1;
    navmode = TRACKBALL;
}

bool unproject (int mouse_x, int mouse_y, vec3 &p) {
    ///===================== DONE =====================
    ///--- TODO H5.1: Screen-space unprojection

    // 1) Compute the inverse of MVP
    mat4 inv_mvp = (projection*view*trackball_matrix*model).inverse();

    // 2) Find new screen-space coordinates from mouse position
    vec2 temp = transform_screen_coords(mouse_x,mouse_y);

    //Previous point in screen coordinates
    vec4 previous_pos_view = projection*view*trackball_matrix*model*vec4(p[0],p[1],p[2],1.0);
    float factor = previous_pos_view[3] == 0 ? 1.0 : previous_pos_view[3];
    previous_pos_view = previous_pos_view * 1.0f/factor;
    vec4 p_screen = vec4(temp[0],temp[1],previous_pos_view[2],1.0f);

    // 3) Obtain object coordinates p
    vec4 new_pos = inv_mvp*p_screen;
    float factor2 = new_pos[3]==0 ? 1.0 : new_pos[3];
    p = (1.0f / factor2)*vec3(new_pos[0],new_pos[1],new_pos[2]);

    return true;
}

void display(){
    opengp::update_title_fps("shading");   
    glViewport(0,0,window_width,window_height);
        
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
    if (navmode == BEZIER) {
        vec3 cam_pos(2.0f, 2.0f, 2.0f);
        vec3 cam_look(0.0f, 0.0f, 0.0f);
        vec3 cam_up(0.0f, 0.0f, 1.0f);

        ///===================== DONE =====================
        ///--- TODO H5.3 sample cam_pos and cam_up from cam_pos_curve
        /// and cam_look_curve.
        /// Use glfwGetTime() and wrap it to [0, 1] to get the curve
        /// parameter.
        ///================================================

        double animation_duration = 10.0f;
        //We compute t according to the current time and the desired duration
        double t = (glfwGetTime() - start_time) / animation_duration;

        //We need to loop the animation correctlywhen t reach 1
        if (t>1.0f){
            t=0.0f;
            start_time = glfwGetTime();
        }

        //Sampling
        cam_pos_curve.sample_point(t,cam_pos);
        cam_look_curve.sample_point(t,cam_look);

        mat4 view_bezier = Eigen::lookAt(cam_pos, cam_look, cam_up);
        quad.draw(model, view_bezier, projection);
        cube.draw(model, view_bezier, projection);
    }

    if (navmode == TRACKBALL) {
        for (unsigned int i = 0; i < cam_pos_points.size(); i++) {
            cam_pos_points[i].draw(trackball_matrix * model, view, projection);
        }

        ///===================== DONE =====================
        ///--- TODO H5.3: Draw control points for cam_look_curve
        /// ===============================================

        for (unsigned int i = 0; i < cam_look_points.size(); i++) {
           cam_look_points[i].draw(trackball_matrix * model, view, projection);
        }

        cam_pos_curve.draw(trackball_matrix * model, view, projection);
        cam_look_curve.draw(trackball_matrix * model, view, projection);

        quad.draw(trackball_matrix * model, view, projection);
        cube.draw(trackball_matrix * model, view, projection);
    }
}

void render_selection() {
    glViewport(0,0,window_width,window_height);

    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
    for (unsigned int i = 0; i < cam_pos_points.size(); i++) {
            cam_pos_points[i].draw_selection(trackball_matrix*model, view, projection);
    }

    ///===================== DONE =====================
    ///--- TODO H5.3 Draw control points for cam_look_curve
    ///================================================
    ///
    for (unsigned int i = 0; i < cam_look_points.size(); i++) {
           cam_look_points[i].draw_selection(trackball_matrix*model, view, projection);
    }
}

void selection_button(int button, int action) {
    static int x_pressed = 0, y_pressed = 0;

    if (button == GLFW_MOUSE_BUTTON_LEFT && action == GLFW_PRESS) {
        int x = 0, y = 0;
        glfwGetMousePos(&x, &y);
        x_pressed = x; y_pressed = y;

        render_selection();

        glFlush();
        glFinish();

        unsigned char res[4];
        glReadPixels(x, window_height - y, 1,1,GL_RGBA, GL_UNSIGNED_BYTE, &res);
        selected_point = res[0];

        if (selected_point >= 0 && selected_point < cam_pos_points.size())
            cam_pos_points[selected_point].selected() = true;

        ///===================== DONE =====================
        ///--- TODO H5.3 Process cam_look_points for selection
        ///================================================

        //We take into account the index shift introduced at the beginning
        if (selected_point >= cam_pos_points.size() && selected_point < cam_look_points.size()+cam_pos_points.size())
            cam_look_points[selected_point-cam_pos_points.size()].selected() = true;
    }

    if (button == GLFW_MOUSE_BUTTON_LEFT && action == GLFW_RELEASE) {
        std::cout << "Selected point: " << selected_point << std::endl;
        if (selected_point >= 0 && selected_point < cam_pos_points.size()) {
            cam_pos_points[selected_point].selected() = false;
        }

        ///===================== DONE =====================
        ///--- TODO H5.3 Process cam_look_points for selection
        ///================================================

        if (selected_point >= cam_pos_points.size() && selected_point < cam_look_points.size()+cam_pos_points.size()) {
            cam_look_points[selected_point-cam_pos_points.size()].selected() = false;
        }

        int x = 0, y = 0;
        glfwGetMousePos(&x, &y);
        if (x == x_pressed && y == y_pressed) {
            return;
        }

        if (selected_point >= 0 && selected_point < cam_pos_points.size()) {
            unproject(x, y, cam_pos_points[selected_point].position());
            cam_pos_curve.set_points(cam_pos_points);
           // std::cout << "Pos: " << cam_pos_points[selected_point].position() << ", ind: " << selected_point << std::endl;
        }

        ///===================== DONE =====================
        ///--- TODO H5.3 Update control points of cam_look_curve
        ///================================================
        if (selected_point >= cam_pos_points.size() && selected_point < cam_look_points.size()+cam_pos_points.size()) {
            unproject(x, y, cam_look_points[selected_point-cam_pos_points.size()].position());
            cam_look_curve.set_points(cam_look_points);
            //std::cout << "Look: " << cam_look_points[selected_point-cam_pos_points.size()].position() << ", ind: "<< selected_point-cam_pos_points.size() << std::endl;
        }
    }
}

void keyboard(int key, int action){
    //std::cout << "Key: " << key << std::endl;
    if(action != GLFW_RELEASE) return; ///< only act on release
    switch(key){
        case '1':
            navmode = TRACKBALL;
            std::cout << "Trackball mode.\n" << std::flush;
            break;
        case '2':
            navmode = BEZIER;
            std::cout << "Bezier mode.\n" << std::flush;
            //We need to start the moment the animation begin, to compute the duration
            start_time = glfwGetTime();
            break;
        default:
            break;
    }
}

void mouse_button(int button, int action) {
    if (navmode == BEZIER) {
        std::cout << "No mouse interaction in Bezier mode.\n" << std::flush;
        return;
    }

    if (glfwGetKey(GLFW_KEY_LSHIFT) == GLFW_PRESS) {
        trackball_button(button, action);
    } else {
        selection_button(button, action);
    }
}

void mouse_pos(int x, int y) {
    if (navmode == BEZIER) {
        return;
    }
    if (glfwGetKey(GLFW_KEY_LSHIFT) == GLFW_PRESS) {
        trackball_pos(x, y);
    }
}
void cleanup(){
    glDeleteProgram(_pid_bezier);
    glDeleteProgram(_pid_point);
    glDeleteProgram(_pid_point_selection);
}

int main(int, char**){
    glfwInitWindowSize(window_width, window_height);
    glfwCreateWindow();
    glfwDisplayFunc(display);
    glfwSetKeyCallback(keyboard);
    glfwSetMouseButtonCallback(mouse_button);
    glfwSetMousePosCallback(mouse_pos);
    init();
    keyboard(GLFW_KEY_KP_1, 0);
    glfwMainLoop();
    cleanup();
    return EXIT_SUCCESS;
}
