#version 330 core
in vec2 uv;
uniform sampler2D colorTex;
uniform sampler2D velocityTex;
out vec4 color;


void main() {
    float interval = 30.0;

    vec4 blurcolor = vec4(0,0,0,0);

    vec2 dimension = textureSize(velocityTex,0);

    vec2 velocity = texture(velocityTex, uv).xy;
   // vec2 velocity = vec2(200,200);

    for(int i= 1;i< int(interval);i++){
       blurcolor += texture(colorTex, uv + vec2( (velocity.x/interval) * i / dimension.x  , (velocity.y/interval) * i / dimension.y ) );
    }


    color = blurcolor/interval;

}

