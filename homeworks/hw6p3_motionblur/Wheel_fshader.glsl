#version 330 core
in vec2 uv;
in vec4 currentPosition;
in vec4 previousPosition;

uniform sampler2D tex;

layout (location = 0) out vec4 color;
layout (location = 1) out vec2 motionVector;

void main() {
    color = texture(tex, uv);
//#define STRIP_CODE
    // TODO: compute the screen space motion vector (in pixels!)
    vec2 size = textureSize(tex,0);

    float width = float(size.x);
    float height = float(size.y);

    float x = (currentPosition.x + 1) * .5  * width ;
    float x_old = (previousPosition.x + 1) * .5  * width;
    float y = (currentPosition.y + 1) * .5  * height;
    float y_old = (previousPosition.y + 1) * .5  * height;

    // HINT: use straightforward finite differences and assume unit time-step
    // HINT: how do you compute pixels positions given homogeneous coordinate? (x,y,z,w)
    //motionVector = vec2((currentPosition.x - previousPosition.x)*width*0.5/currentPosition.w,(currentPosition.y - previousPosition.y)*height*0.5/currentPosition.w);
    motionVector = vec2(x - x_old , y - y_old )/ currentPosition.w / currentPosition.z;

}
